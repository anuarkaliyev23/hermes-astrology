package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import org.junit.jupiter.api.Test
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.test.assertEquals


internal class HermesSwissEphemerisWrapperTest {
    val wrapper = HermesSwissEphemerisWrapper(JnaSwissEphemerisLibraryFactory("sweph\\swedll64.dll"))
    val exampleDate = ZonedDateTime.of(1997, 5, 20, 9, 59, 0, 0, ZoneId.of("Asia/Almaty"))
    val exampleLatitude = 43.238949
    val exampleLongitude = 76.889709

    @Test
    fun planetOf() {
        val sun = wrapper.planetOf(exampleDate, PlanetType.SUN)
        assertEquals(sun.position.coordinate.toInt(), 59)

        val moon = wrapper.planetOf(exampleDate, PlanetType.MOON)
        assertEquals(moon.position.coordinate.toInt(), 211)

        val mercury = wrapper.planetOf(exampleDate, PlanetType.MERCURY)
        assertEquals(mercury.position.coordinate.toInt(), 34)

        val venus = wrapper.planetOf(exampleDate, PlanetType.VENUS)
        assertEquals(venus.position.coordinate.toInt(), 71)

        val mars = wrapper.planetOf(exampleDate, PlanetType.MARS)
        assertEquals(mars.position.coordinate.toInt(), 169)

        val jupiter = wrapper.planetOf(exampleDate, PlanetType.JUPITER)
        assertEquals(jupiter.position.coordinate.toInt(), 321)

        val saturn = wrapper.planetOf(exampleDate, PlanetType.SATURN)
        assertEquals(saturn.position.coordinate.toInt(), 16)

        val uranus = wrapper.planetOf(exampleDate, PlanetType.URANUS)
        assertEquals(uranus.position.coordinate.toInt(), 308)

        val neptune = wrapper.planetOf(exampleDate, PlanetType.NEPTUNE)
        assertEquals(neptune.position.coordinate.toInt(), 299)

        val pluto = wrapper.planetOf(exampleDate, PlanetType.PLUTO)
        assertEquals(pluto.position.coordinate.toInt(), 244)

    }

    @Test
    fun housesOf() {
        val grid = wrapper.housesOf(exampleDate, exampleLatitude, exampleLongitude, HouseSystem.PLACIDUS)
        assertEquals(grid[HouseIndex.I].position.coordinate.toInt(), 110)
        assertEquals(grid[HouseIndex.II].position.coordinate.toInt(), 128)
        assertEquals(grid[HouseIndex.III].position.coordinate.toInt(), 150)
        assertEquals(grid[HouseIndex.IV].position.coordinate.toInt(), 179)
        assertEquals(grid[HouseIndex.V].position.coordinate.toInt(), 216)
        assertEquals(grid[HouseIndex.VI].position.coordinate.toInt(), 256)
        assertEquals(grid[HouseIndex.VII].position.coordinate.toInt(), 290)
        assertEquals(grid[HouseIndex.VIII].position.coordinate.toInt(), 308)
        assertEquals(grid[HouseIndex.IX].position.coordinate.toInt(), 330)
        assertEquals(grid[HouseIndex.X].position.coordinate.toInt(), 359)
        assertEquals(grid[HouseIndex.XI].position.coordinate.toInt(), 36)
        assertEquals(grid[HouseIndex.XII].position.coordinate.toInt(), 76)

    }

    @Test
    fun iflagTest() {
        assertEquals(SwissEphemerisLibrary.IFLAG_ALL, 256)
    }

    @Test
    fun julianDayUT() {
        assertEquals(wrapper.julianDayUT(exampleDate), 2450588.6243012124)
    }
}