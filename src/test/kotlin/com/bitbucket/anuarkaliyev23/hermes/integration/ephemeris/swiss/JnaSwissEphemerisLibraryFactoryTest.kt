package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime

internal class JnaSwissEphemerisLibraryFactoryTest {
    val library = JnaSwissEphemerisLibraryFactory("sweph\\swedll64.dll").library()

    @Test
    fun swe_calc_ut() {
        val array = arrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0).toDoubleArray()
        val a = library.swe_calc_ut(2450588.6251294445, 1, 256, array, CharArray(40))
        println(a)
        println(array.toList())
    }

    @Test
    fun swe_utc_to_jd() {
        val date = ZonedDateTime.of(LocalDateTime.of(1997, 5, 20, 9, 59,9), ZoneId.of("Asia/Almaty")).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()
        println(date)
        val jdArray = arrayOf(0.0, 0.0).toDoubleArray()
        val serr = arrayOf(' ').toCharArray()
        val result = library.swe_utc_to_jd(date.year, date.monthValue, date.dayOfMonth, date.hour, date.minute, date.second.toDouble(), 1, jdArray, serr)
        println(result)
        println(jdArray.toList())
        println(serr)
    }

    @Test
    fun swe_houses() {
        val latitude = 43.238949
        val longitude = 76.889709
        val houses = arrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0).toDoubleArray()
        val ascmc = arrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0).toDoubleArray()
        val houseSystem = 'P'.toInt()
        val houseResult = library.swe_houses(2450588.6251294445, latitude, longitude, houseSystem, houses, ascmc)
        println("housesOf: ${houses.toList()}")
        println("asmc: ${ascmc.toList()}")
        println(houseResult)
    }
}