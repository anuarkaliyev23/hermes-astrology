package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import org.junit.jupiter.api.Test
import java.time.ZoneId

import java.time.ZonedDateTime

internal class SwissEphemerisChartFactoryTest {
    val wrapper = HermesSwissEphemerisWrapper(JnaSwissEphemerisLibraryFactory("sweph\\swedll64.dll"))
    val factory = SwissEphemerisChartFactory(wrapper)

    val exampleDate = ZonedDateTime.of(1997, 5, 20, 9, 59, 0, 0, ZoneId.of("Asia/Almaty"))
    val exampleLatitude = 43.238949
    val exampleLongitude = 76.889709

    @Test
    fun cosmogram() {
        println(factory.cosmogram(ZonedDateTime.now()))
    }

    @Test
    fun houseChart() {
        println(factory.houseChart(exampleDate, exampleLatitude, exampleLongitude, HouseSystem.PLACIDUS))
    }

    @Test
    fun houses() {
        println(factory.houses(exampleDate, exampleLatitude, exampleLongitude, HouseSystem.PLACIDUS))
    }
}