package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class HouseIndexTest {

    @Test
    fun oppositeIndex() {
        assertEquals(HouseIndex.I, HouseIndex.VII.oppositeIndex())
        assertEquals(HouseIndex.II, HouseIndex.VIII.oppositeIndex())
        assertEquals(HouseIndex.III, HouseIndex.IX.oppositeIndex())
        assertEquals(HouseIndex.IV, HouseIndex.X.oppositeIndex())
        assertEquals(HouseIndex.V, HouseIndex.XI.oppositeIndex())
        assertEquals(HouseIndex.VI, HouseIndex.XII.oppositeIndex())
        assertEquals(HouseIndex.VII, HouseIndex.I.oppositeIndex())
        assertEquals(HouseIndex.VIII, HouseIndex.II.oppositeIndex())
        assertEquals(HouseIndex.IX, HouseIndex.III.oppositeIndex())
        assertEquals(HouseIndex.X, HouseIndex.IV.oppositeIndex())
        assertEquals(HouseIndex.XI, HouseIndex.V.oppositeIndex())
        assertEquals(HouseIndex.XII, HouseIndex.VI.oppositeIndex())
    }

    @Test
    fun nextIndex() {
        assertEquals(HouseIndex.I, HouseIndex.XII.nextIndex())
        assertEquals(HouseIndex.II, HouseIndex.I.nextIndex())
        assertEquals(HouseIndex.III, HouseIndex.II.nextIndex())
        assertEquals(HouseIndex.IV, HouseIndex.III.nextIndex())
        assertEquals(HouseIndex.V, HouseIndex.IV.nextIndex())
        assertEquals(HouseIndex.VI, HouseIndex.V.nextIndex())
        assertEquals(HouseIndex.VII, HouseIndex.VI.nextIndex())
        assertEquals(HouseIndex.VIII, HouseIndex.VII.nextIndex())
        assertEquals(HouseIndex.IX, HouseIndex.VIII.nextIndex())
        assertEquals(HouseIndex.X, HouseIndex.IX.nextIndex())
        assertEquals(HouseIndex.XI, HouseIndex.X.nextIndex())
        assertEquals(HouseIndex.XII, HouseIndex.XI.nextIndex())
    }
}