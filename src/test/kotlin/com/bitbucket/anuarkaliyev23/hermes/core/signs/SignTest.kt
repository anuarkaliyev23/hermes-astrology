package com.bitbucket.anuarkaliyev23.hermes.core.signs

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals


class SignTest {



    @Test
    fun nextSign() {
        assertEquals(Sign.ARIES,        Sign.nextSign(Sign.PISCES))
        assertEquals(Sign.TAURUS,       Sign.nextSign(Sign.ARIES))
        assertEquals(Sign.GEMINI,       Sign.nextSign(Sign.TAURUS))
        assertEquals(Sign.CANCER,       Sign.nextSign(Sign.GEMINI))
        assertEquals(Sign.LEO,          Sign.nextSign(Sign.CANCER))
        assertEquals(Sign.VIRGO,        Sign.nextSign(Sign.LEO))
        assertEquals(Sign.LIBRA,        Sign.nextSign(Sign.VIRGO))
        assertEquals(Sign.SCORPIO,      Sign.nextSign(Sign.LIBRA))
        assertEquals(Sign.SAGITTARIUS,  Sign.nextSign(Sign.SCORPIO))
        assertEquals(Sign.CAPRICORN,    Sign.nextSign(Sign.SAGITTARIUS))
        assertEquals(Sign.AQUARIUS,     Sign.nextSign(Sign.CAPRICORN))
        assertEquals(Sign.PISCES,       Sign.nextSign(Sign.AQUARIUS))
    }

    @Test
    fun prevSign() {
        assertEquals(Sign.ARIES,        Sign.prevSign(Sign.TAURUS))
        assertEquals(Sign.TAURUS,       Sign.prevSign(Sign.GEMINI))
        assertEquals(Sign.GEMINI,       Sign.prevSign(Sign.CANCER))
        assertEquals(Sign.CANCER,       Sign.prevSign(Sign.LEO))
        assertEquals(Sign.LEO,          Sign.prevSign(Sign.VIRGO))
        assertEquals(Sign.VIRGO,        Sign.prevSign(Sign.LIBRA))
        assertEquals(Sign.LIBRA,        Sign.prevSign(Sign.SCORPIO))
        assertEquals(Sign.SCORPIO,      Sign.prevSign(Sign.SAGITTARIUS))
        assertEquals(Sign.SAGITTARIUS,  Sign.prevSign(Sign.CAPRICORN))
        assertEquals(Sign.CAPRICORN,    Sign.prevSign(Sign.AQUARIUS))
        assertEquals(Sign.AQUARIUS,     Sign.prevSign(Sign.PISCES))
        assertEquals(Sign.PISCES,       Sign.prevSign(Sign.ARIES))
    }

    @Test
    fun signOf() {
        assertEquals(Sign.ARIES,        Sign.of(0.0))
        assertEquals(Sign.TAURUS,       Sign.of(30.0))
        assertEquals(Sign.GEMINI,       Sign.of(60.0))
        assertEquals(Sign.CANCER,       Sign.of(90.0))
        assertEquals(Sign.LEO,          Sign.of(120.0))
        assertEquals(Sign.VIRGO,        Sign.of(150.0))
        assertEquals(Sign.LIBRA,        Sign.of(180.0))
        assertEquals(Sign.SCORPIO,      Sign.of(210.0))
        assertEquals(Sign.SAGITTARIUS,  Sign.of(240.0))
        assertEquals(Sign.CAPRICORN,    Sign.of(270.0))
        assertEquals(Sign.AQUARIUS,     Sign.of(300.0))
        assertEquals(Sign.PISCES,       Sign.of(330.0))

        assertEquals(Sign.ARIES,        Sign.of(360.0))
        assertEquals(Sign.TAURUS,       Sign.of(390.0))
        assertEquals(Sign.GEMINI,       Sign.of(420.0))
        assertEquals(Sign.CANCER,       Sign.of(450.0))
        assertEquals(Sign.LEO,          Sign.of(480.0))
        assertEquals(Sign.VIRGO,        Sign.of(510.0))
        assertEquals(Sign.LIBRA,        Sign.of(540.0))
        assertEquals(Sign.SCORPIO,      Sign.of(570.0))
        assertEquals(Sign.SAGITTARIUS,  Sign.of(600.0))
        assertEquals(Sign.CAPRICORN,    Sign.of(630.0))
        assertEquals(Sign.AQUARIUS,     Sign.of(660.0))
        assertEquals(Sign.PISCES,       Sign.of(690.0))

        assertEquals(Sign.ARIES,        Sign.of(-360.0))
        assertEquals(Sign.TAURUS,       Sign.of(-330.0))
        assertEquals(Sign.GEMINI,       Sign.of(-300.0))
        assertEquals(Sign.CANCER,       Sign.of(-270.0))
        assertEquals(Sign.LEO,          Sign.of(-240.0))
        assertEquals(Sign.VIRGO,        Sign.of(-210.0))
        assertEquals(Sign.LIBRA,        Sign.of(-180.0))
        assertEquals(Sign.SCORPIO,      Sign.of(-150.0))
        assertEquals(Sign.SAGITTARIUS,  Sign.of(-120.0))
        assertEquals(Sign.CAPRICORN,    Sign.of(-90.0))
        assertEquals(Sign.AQUARIUS,     Sign.of(-60.0))
        assertEquals(Sign.PISCES,       Sign.of(-30.0))
    }

    @Test
    fun oppositeSign() {
        assertEquals(Sign.LIBRA,        Sign.oppositeSign(Sign.ARIES))
        assertEquals(Sign.SCORPIO,      Sign.oppositeSign(Sign.TAURUS))
        assertEquals(Sign.SAGITTARIUS,  Sign.oppositeSign(Sign.GEMINI))
        assertEquals(Sign.CAPRICORN,    Sign.oppositeSign(Sign.CANCER))
        assertEquals(Sign.AQUARIUS,     Sign.oppositeSign(Sign.LEO))
        assertEquals(Sign.PISCES,       Sign.oppositeSign(Sign.VIRGO))
        assertEquals(Sign.ARIES,        Sign.oppositeSign(Sign.LIBRA))
        assertEquals(Sign.TAURUS,       Sign.oppositeSign(Sign.SCORPIO))
        assertEquals(Sign.GEMINI,       Sign.oppositeSign(Sign.SAGITTARIUS))
        assertEquals(Sign.CANCER,       Sign.oppositeSign(Sign.CAPRICORN))
        assertEquals(Sign.LEO,          Sign.oppositeSign(Sign.AQUARIUS))
        assertEquals(Sign.VIRGO,        Sign.oppositeSign(Sign.PISCES))
    }

    @Test
    fun symbol() {
        println("${Sign.ARIES} ${Sign.ARIES.symbol}")
        assertEquals(Sign.ARIES.symbol, "\u2648")

        println("${Sign.TAURUS} ${Sign.TAURUS.symbol}")
        assertEquals(Sign.TAURUS.symbol, "\u2649")

        println("${Sign.GEMINI} ${Sign.GEMINI.symbol}")
        assertEquals(Sign.GEMINI.symbol, "\u264a")

        println("${Sign.CANCER} ${Sign.CANCER.symbol}")
        assertEquals(Sign.CANCER.symbol, "\u264b")

        println("${Sign.LEO} ${Sign.LEO.symbol}")
        assertEquals(Sign.LEO.symbol, "\u264c")

        println("${Sign.VIRGO} ${Sign.VIRGO.symbol}")
        assertEquals(Sign.VIRGO.symbol, "\u264d")

        println("${Sign.LIBRA} ${Sign.LIBRA.symbol}")
        assertEquals(Sign.LIBRA.symbol, "\u264e")

        println("${Sign.SCORPIO} ${Sign.SCORPIO.symbol}")
        assertEquals(Sign.SCORPIO.symbol, "\u264f")

        println("${Sign.SAGITTARIUS} ${Sign.SAGITTARIUS.symbol}")
        assertEquals(Sign.SAGITTARIUS.symbol, "\u2650")

        println("${Sign.CAPRICORN} ${Sign.CAPRICORN.symbol}")
        assertEquals(Sign.CAPRICORN.symbol, "\u2651")

        println("${Sign.AQUARIUS} ${Sign.AQUARIUS.symbol}")
        assertEquals(Sign.AQUARIUS.symbol, "\u2652")

        println("${Sign.PISCES} ${Sign.PISCES.symbol}")
        assertEquals(Sign.PISCES.symbol, "\u2653")
    }
}