package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import org.junit.jupiter.api.Test
import java.time.ZonedDateTime

internal class CosmogramTest {

    @Test
    fun aspects() {
        val planets = ArrayList<Planet>()
        planets.add(Planet(PlanetType.SUN,      33.5472904.toHoroscopePosition(), 0.974))
        planets.add(Planet(PlanetType.MOON,     270.7263526.toHoroscopePosition(), 12.643341))
        planets.add(Planet(PlanetType.MERCURY,  9.0241560.toHoroscopePosition(), 1.447726))
        planets.add(Planet(PlanetType.VENUS,    4.0384094.toHoroscopePosition(), 1.211411))
        planets.add(Planet(PlanetType.MARS,     75.6238218.toHoroscopePosition(), 0.653383))
        planets.add(Planet(PlanetType.JUPITER,  264.07531817.toHoroscopePosition(), -0.041011))
        planets.add(Planet(PlanetType.SATURN,   290.4888341.toHoroscopePosition(), 0.009920))
        planets.add(Planet(PlanetType.URANUS,   32.5879720.toHoroscopePosition(), 0.057514))
        planets.add(Planet(PlanetType.NEPTUNE,  347.8324297.toHoroscopePosition(), 0.028728))
        planets.add(Planet(PlanetType.PLUTO,    293.1515704.toHoroscopePosition(), 0.000380))

        val cosmogram = Cosmogram(planets, ZonedDateTime.now())

        val orbTable = com.bitbucket.anuarkaliyev23.hermes.core.aspects.PlanetBasedOrbTable(
                planetToOrbMap = mapOf(
                        PlanetType.SUN to 5.0,
                        PlanetType.MOON to 5.0,
                        PlanetType.MERCURY to 1.0,
                        PlanetType.VENUS to 1.0,
                        PlanetType.MARS to 1.0,
                        PlanetType.JUPITER to 3.0,
                        PlanetType.SATURN to 3.0,
                        PlanetType.URANUS to 1.0,
                        PlanetType.NEPTUNE to 1.0,
                        PlanetType.PLUTO to 1.0
                )
        )

        cosmogram.aspects(orbTable).filter { it.type() != null }.forEach {
            println(it)
        }

    }
}