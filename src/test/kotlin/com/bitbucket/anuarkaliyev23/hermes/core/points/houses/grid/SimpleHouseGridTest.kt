package com.bitbucket.anuarkaliyev23.hermes.core.points.houses.grid

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.House
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.SimpleHouseGrid
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.util.*

internal class SimpleHouseGridTest {

    @Test
    fun testInitNoHouse() {
        val houses = TreeSet<House>()
        houses.add(House(HouseIndex.I, HoroscopePosition(0.0)))
        houses.add(House(HouseIndex.II, HoroscopePosition(30.0)))
        houses.add(House(HouseIndex.III, HoroscopePosition(60.0)))
        houses.add(House(HouseIndex.IV, HoroscopePosition(90.0)))
        houses.add(House(HouseIndex.V, HoroscopePosition(120.0)))
        houses.add(House(HouseIndex.VI, HoroscopePosition(150.0)))
        houses.add(House(HouseIndex.VII, HoroscopePosition(180.0)))
        houses.add(House(HouseIndex.VIII, HoroscopePosition(210.0)))
        houses.add(House(HouseIndex.IX, HoroscopePosition(240.0)))
        houses.add(House(HouseIndex.X, HoroscopePosition(270.0)))
        houses.add(House(HouseIndex.XI, HoroscopePosition(300.0)))
//        houses.add(House(HouseIndex.XII, HoroscopePosition(330.0)))

        assertThrows(com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house.IncompleteHouseGridException::class.java) { SimpleHouseGrid(houses) }
    }

    @Test
    fun testInitRepeatingHouseIndex() {
        val houses = TreeSet<House>()
        houses.add(House(HouseIndex.I, HoroscopePosition(0.0)))
        houses.add(House(HouseIndex.II, HoroscopePosition(30.0)))
        houses.add(House(HouseIndex.III, HoroscopePosition(60.0)))
        houses.add(House(HouseIndex.IV, HoroscopePosition(90.0)))
        houses.add(House(HouseIndex.V, HoroscopePosition(120.0)))
        houses.add(House(HouseIndex.VI, HoroscopePosition(150.0)))
        houses.add(House(HouseIndex.VII, HoroscopePosition(180.0)))
        houses.add(House(HouseIndex.VIII, HoroscopePosition(210.0)))
        houses.add(House(HouseIndex.IX, HoroscopePosition(240.0)))
        houses.add(House(HouseIndex.X, HoroscopePosition(270.0)))
        houses.add(House(HouseIndex.XI, HoroscopePosition(300.0)))
        houses.add(House(HouseIndex.XI, HoroscopePosition(330.0)))

        assertThrows(com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house.IncompleteHouseGridException::class.java) { SimpleHouseGrid(houses) }
    }


    @Test
    fun get() {
        val houses = TreeSet<House>()
        houses.add(House(HouseIndex.I, HoroscopePosition(0.0)))
        houses.add(House(HouseIndex.II, HoroscopePosition(30.0)))
        houses.add(House(HouseIndex.III, HoroscopePosition(60.0)))
        houses.add(House(HouseIndex.IV, HoroscopePosition(90.0)))
        houses.add(House(HouseIndex.V, HoroscopePosition(120.0)))
        houses.add(House(HouseIndex.VI, HoroscopePosition(150.0)))
        houses.add(House(HouseIndex.VII, HoroscopePosition(180.0)))
        houses.add(House(HouseIndex.VIII, HoroscopePosition(210.0)))
        houses.add(House(HouseIndex.IX, HoroscopePosition(240.0)))
        houses.add(House(HouseIndex.X, HoroscopePosition(270.0)))
        houses.add(House(HouseIndex.XI, HoroscopePosition(300.0)))
        houses.add(House(HouseIndex.XII, HoroscopePosition(330.0)))

        val placidus = SimpleHouseGrid(houses)

        assertEquals(placidus[HouseIndex.I].position, HoroscopePosition(0.0))
        assertEquals(placidus[HouseIndex.II].position, HoroscopePosition(30.0))
        assertEquals(placidus[HouseIndex.III].position, HoroscopePosition(60.0))
        assertEquals(placidus[HouseIndex.IV].position, HoroscopePosition(90.0))
        assertEquals(placidus[HouseIndex.V].position, HoroscopePosition(120.0))
        assertEquals(placidus[HouseIndex.VI].position, HoroscopePosition(150.0))
        assertEquals(placidus[HouseIndex.VII].position, HoroscopePosition(180.0))
        assertEquals(placidus[HouseIndex.VIII].position, HoroscopePosition(210.0))
        assertEquals(placidus[HouseIndex.IX].position, HoroscopePosition(240.0))
        assertEquals(placidus[HouseIndex.X].position, HoroscopePosition(270.0))
        assertEquals(placidus[HouseIndex.XI].position, HoroscopePosition(300.0))
        assertEquals(placidus[HouseIndex.XII].position, HoroscopePosition(330.0))
    }

    @Test
    fun houseOf() {
        val houses = TreeSet<House>()
        houses.add(House(HouseIndex.I, HoroscopePosition(0.0)))
        houses.add(House(HouseIndex.II, HoroscopePosition(30.0)))
        houses.add(House(HouseIndex.III, HoroscopePosition(60.0)))
        houses.add(House(HouseIndex.IV, HoroscopePosition(90.0)))
        houses.add(House(HouseIndex.V, HoroscopePosition(120.0)))
        houses.add(House(HouseIndex.VI, HoroscopePosition(150.0)))
        houses.add(House(HouseIndex.VII, HoroscopePosition(180.0)))
        houses.add(House(HouseIndex.VIII, HoroscopePosition(210.0)))
        houses.add(House(HouseIndex.IX, HoroscopePosition(240.0)))
        houses.add(House(HouseIndex.X, HoroscopePosition(270.0)))
        houses.add(House(HouseIndex.XI, HoroscopePosition(300.0)))
        houses.add(House(HouseIndex.XII, HoroscopePosition(330.0)))

        val placidus = SimpleHouseGrid(houses)

        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 0.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 1.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 2.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 3.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 4.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 5.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 6.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 7.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 8.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 9.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 10.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 11.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 12.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 13.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 14.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 15.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 16.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 17.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 18.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 19.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 20.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 21.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 22.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 23.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 24.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 25.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 26.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 27.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 28.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 29.0.toHoroscopePosition(), 0.0)).index, HouseIndex.I)


        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 31.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 32.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 33.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 34.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 35.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 36.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 37.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 38.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 39.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 40.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 41.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 42.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 43.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 44.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 45.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 46.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 47.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 48.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 49.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 50.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 51.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 52.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 53.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 54.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 55.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 56.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 57.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 58.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)
        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 59.0.toHoroscopePosition(), 0.0)).index, HouseIndex.II)


        assertEquals(placidus.houseOf(Planet(PlanetType.SUN, 60.0.toHoroscopePosition(), 0.0)).index, HouseIndex.III)

    }
}