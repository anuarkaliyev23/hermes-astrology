package com.bitbucket.anuarkaliyev23.hermes.core.aspects

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AspectTest {

    @Test
    fun aspectType() {
        val first = Planet(PlanetType.SUN, 0.toHoroscopePosition(), 1.0)
        val second = Planet(PlanetType.MOON, 0.toHoroscopePosition(), 13.0)

        assertEquals(AspectType.CONJUNCTION, Aspect(first, second, 5.0).type())
        second.position = 359.toHoroscopePosition()
        assertEquals(AspectType.CONJUNCTION, Aspect(first, second, 5.0).type())
        second.position = 1.0.toHoroscopePosition()
        assertEquals(AspectType.CONJUNCTION, Aspect(first, second, 5.0).type())


        second.position = 39.toHoroscopePosition()
        assertEquals(AspectType.SEMISEXTILE, Aspect(first, second, 1.0).type())
        second.position = 40.toHoroscopePosition()
        assertEquals(AspectType.SEMISEXTILE, Aspect(first, second, 1.0).type())
        second.position = 41.toHoroscopePosition()
        assertEquals(AspectType.SEMISEXTILE, Aspect(first, second, 1.0).type())

        second.position = 44.toHoroscopePosition()
        assertEquals(AspectType.SEMISQUARE, Aspect(first, second, 1.0).type())
        second.position = 45.toHoroscopePosition()
        assertEquals(AspectType.SEMISQUARE, Aspect(first, second, 1.0).type())
        second.position = 46.toHoroscopePosition()
        assertEquals(AspectType.SEMISQUARE, Aspect(first, second, 1.0).type())


        second.position = 59.toHoroscopePosition()
        assertEquals(AspectType.SEXTILE, Aspect(first, second, 1.0).type())
        second.position = 60.toHoroscopePosition()
        assertEquals(AspectType.SEXTILE, Aspect(first, second, 1.0).type())
        second.position = 61.toHoroscopePosition()
        assertEquals(AspectType.SEXTILE, Aspect(first, second, 1.0).type())

        second.position = 71.toHoroscopePosition()
        assertEquals(AspectType.QUINTILE, Aspect(first, second, 1.0).type())
        second.position = 72.toHoroscopePosition()
        assertEquals(AspectType.QUINTILE, Aspect(first, second, 1.0).type())
        second.position = 73.toHoroscopePosition()
        assertEquals(AspectType.QUINTILE, Aspect(first, second, 1.0).type())

        second.position = 89.toHoroscopePosition()
        assertEquals(AspectType.SQUARE, Aspect(first, second, 1.0).type())
        second.position = 90.toHoroscopePosition()
        assertEquals(AspectType.SQUARE, Aspect(first, second, 1.0).type())
        second.position = 91.toHoroscopePosition()
        assertEquals(AspectType.SQUARE, Aspect(first, second, 1.0).type())

        second.position = 119.toHoroscopePosition()
        assertEquals(AspectType.TRINE, Aspect(first, second, 1.0).type())
        second.position = 120.toHoroscopePosition()
        assertEquals(AspectType.TRINE, Aspect(first, second, 1.0).type())
        second.position = 121.toHoroscopePosition()
        assertEquals(AspectType.TRINE, Aspect(first, second, 1.0).type())


        second.position = 179.toHoroscopePosition()
        assertEquals(AspectType.OPPOSITION, Aspect(first, second, 1.0).type())
        second.position = 180.toHoroscopePosition()
        assertEquals(AspectType.OPPOSITION, Aspect(first, second, 1.0).type())
        second.position = 181.toHoroscopePosition()
        assertEquals(AspectType.OPPOSITION, Aspect(first, second, 1.0).type())
    }

    @Test
    fun isApplicational() {
        val first = Planet(PlanetType.SUN, 0.toHoroscopePosition(), 1.0)
        val second = Planet(PlanetType.MOON, 0.toHoroscopePosition(), 13.0)

        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 359.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 1.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 39.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 40.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 41.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 44.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 45.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 46.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 59.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 60.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 61.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 89.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 90.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 91.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 71.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 72.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 73.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 119.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 120.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 121.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1

        second.position = 179.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 180.toHoroscopePosition()
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.position = 181.toHoroscopePosition()
        assertFalse(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
        assertTrue(Aspect(first, second, 1.0).isApplicational!!)
        second.speed *= -1
    }

    @Test
    fun hasPoint() {
        val first = Planet(PlanetType.SUN, 0.toHoroscopePosition(), 1.0)
        val second = Planet(PlanetType.MOON, 0.toHoroscopePosition(), 13.0)
        val aspect = Aspect(first, second , 1.0)
        println(aspect.points())
        assertTrue { aspect.contains(PlanetType.SUN) }
        assertTrue { aspect.contains(PlanetType.MOON) }
        assertFalse { aspect.contains(PlanetType.MERCURY) }
        assertFalse { aspect.contains(PlanetType.VENUS) }
        assertFalse { aspect.contains(PlanetType.MARS) }
        assertFalse { aspect.contains(PlanetType.JUPITER) }
        assertFalse { aspect.contains(PlanetType.SATURN) }
        assertFalse { aspect.contains(PlanetType.URANUS) }
        assertFalse { aspect.contains(PlanetType.NEPTUNE) }
        assertFalse { aspect.contains(PlanetType.PLUTO) }

    }
}