package com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic

import com.bitbucket.anuarkaliyev23.hermes.core.points.ApheticMark
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PlanetTypeTest {

    @Test
    fun sunApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.SUN
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun moonApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.MOON
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun mercuryApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.MERCURY
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun venusApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.VENUS
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun marsApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.MARS
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun jupiterApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.JUPITER
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun saturnApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.SATURN
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }


    @Test
    fun uranusApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.URANUS
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }


    @Test
    fun neptuneApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.NEPTUNE
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }


    @Test
    fun plutoApheticMapAdequateOppositeMarks() {
        val planet = PlanetType.PLUTO
        val map = planet.apheticMap()
        map.forEach { key, value ->
            assertEquals(value.oppositeMark(), map[key.oppositeSign()])
        }
    }

    @Test
    fun ariesAdequateApheticMark() {
        val sign = Sign.ARIES

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, mars.apheticMap()[sign])
        assertEquals(ApheticMark.DOMICILE, pluto.apheticMap()[sign])
        assertEquals(ApheticMark.EXALTATION, sun.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, neptune.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, moon.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, uranus.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.FALL, saturn.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, venus.apheticMap()[sign])
    }

    @Test
    fun taurusAdequateApheticMark() {
        val sign = Sign.TAURUS

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, venus.apheticMap()[sign])
        assertEquals(ApheticMark.EXALTATION, moon.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, saturn.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, sun.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, neptune.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.FALL, uranus.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, mars.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, pluto.apheticMap()[sign])
    }

    @Test
    fun geminiAdequateApheticMark() {
        val sign = Sign.GEMINI

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, venus.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, saturn.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, uranus.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, moon.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, pluto.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, mars.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, sun.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, neptune.apheticMap()[sign])
    }

    @Test
    fun cancerAdequateApheticMark() {
        val sign = Sign.CANCER

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, moon.apheticMap()[sign])
        assertEquals(ApheticMark.EXALTATION, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, pluto.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, neptune.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, sun.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, venus.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.FALL, mars.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, saturn.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, uranus.apheticMap()[sign])
    }

    @Test
    fun leoAdequateApheticMark() {
        val sign = Sign.LEO

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, sun.apheticMap()[sign])
        assertEquals(ApheticMark.EXALTATION, pluto.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, mars.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, moon.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, venus.apheticMap()[sign])
        assertEquals(ApheticMark.FALL, neptune.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, uranus.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, saturn.apheticMap()[sign])
    }

    @Test
    fun virgoAdequateApheticMark() {
        val sign = Sign.VIRGO

        val sun = PlanetType.SUN
        val moon = PlanetType.MOON
        val mercury = PlanetType.MERCURY
        val venus = PlanetType.VENUS
        val mars = PlanetType.MARS
        val jupiter = PlanetType.JUPITER
        val saturn = PlanetType.SATURN
        val uranus = PlanetType.URANUS
        val neptune = PlanetType.NEPTUNE
        val pluto = PlanetType.PLUTO

        assertEquals(ApheticMark.DOMICILE, mercury.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, uranus.apheticMap()[sign])
        assertEquals(ApheticMark.KINSHIP, saturn.apheticMap()[sign])
        assertEquals(ApheticMark.NEUTRAL, sun.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, moon.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, pluto.apheticMap()[sign])
        assertEquals(ApheticMark.FEUD, mars.apheticMap()[sign])
        assertEquals(ApheticMark.FALL, venus.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, jupiter.apheticMap()[sign])
        assertEquals(ApheticMark.DETRIMENT, neptune.apheticMap()[sign])
    }

//    @Test
//    fun AdequateApheticMark() {
//        val sign = Sign.
//
//        val sun = PlanetType.SUN
//        val moon = PlanetType.MOON
//        val mercury = PlanetType.MERCURY
//        val venus = PlanetType.VENUS
//        val mars = PlanetType.MARS
//        val jupiter = PlanetType.JUPITER
//        val saturn = PlanetType.SATURN
//        val uranus = PlanetType.URANUS
//        val neptune = PlanetType.NEPTUNE
//        val pluto = PlanetType.PLUTO
//
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//        assertEquals(ApheticMark., .apheticMap()[sign])
//    }


}