package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.House
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.SimpleHouseGrid
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.ZonedDateTime
import java.util.*
import kotlin.collections.ArrayList

internal class NatalChartTest {

    @Test
    fun pointsInHouse() {
        val planetList = ArrayList<Planet>()
        planetList.add(Planet(PlanetType.SUN,       HoroscopePosition.of(6, Sign.TAURUS, 36), 0.0))
        planetList.add(Planet(PlanetType.MOON,      HoroscopePosition.of(9, Sign.AQUARIUS, 1), 0.0))
        planetList.add(Planet(PlanetType.MERCURY,   HoroscopePosition.of(13, Sign.ARIES, 44), 0.0))
        planetList.add(Planet(PlanetType.VENUS,     HoroscopePosition.of(7, Sign.ARIES, 51), 0.0))
        planetList.add(Planet(PlanetType.MARS,      HoroscopePosition.of(17, Sign.GEMINI, 40), 0.0))
        planetList.add(Planet(PlanetType.JUPITER,   HoroscopePosition.of(23, Sign.SAGITTARIUS, 56), 0.0))
        planetList.add(Planet(PlanetType.SATURN,    HoroscopePosition.of(20, Sign.CAPRICORN, 31), 0.0))
        planetList.add(Planet(PlanetType.URANUS,    HoroscopePosition.of(2, Sign.TAURUS, 46), 0.0))
        planetList.add(Planet(PlanetType.NEPTUNE,   HoroscopePosition.of(17, Sign.PISCES, 55), 0.0))
        planetList.add(Planet(PlanetType.PLUTO,     HoroscopePosition.of(23, Sign.CAPRICORN, 9), 0.0))

        val houseSet = TreeSet<House>()
        houseSet.add(House(HouseIndex.I,    HoroscopePosition.of(8, Sign.CANCER, 53)))
        houseSet.add(House(HouseIndex.II,   HoroscopePosition.of(27, Sign.CANCER, 43)))
        houseSet.add(House(HouseIndex.III,  HoroscopePosition.of(18, Sign.LEO, 30)))
        houseSet.add(House(HouseIndex.IV,   HoroscopePosition.of(14, Sign.VIRGO, 46)))
        houseSet.add(House(HouseIndex.V,    HoroscopePosition.of(19, Sign.LIBRA, 59)))
        houseSet.add(House(HouseIndex.VI,   HoroscopePosition.of(1, Sign.SAGITTARIUS, 49)))

        houseSet.add(House(HouseIndex.VII,    HoroscopePosition.of(8, Sign.CAPRICORN, 53)))
        houseSet.add(House(HouseIndex.VIII,   HoroscopePosition.of(27, Sign.CAPRICORN, 43)))
        houseSet.add(House(HouseIndex.IX,  HoroscopePosition.of(18, Sign.AQUARIUS, 30)))
        houseSet.add(House(HouseIndex.X,   HoroscopePosition.of(14, Sign.PISCES, 46)))
        houseSet.add(House(HouseIndex.XI,    HoroscopePosition.of(19, Sign.ARIES, 59)))
        houseSet.add(House(HouseIndex.XII,   HoroscopePosition.of(1, Sign.GEMINI, 49)))

        val houseGrid = SimpleHouseGrid(houseSet, HouseSystem.PLACIDUS)

        val natal = NatalChart(planetList, houseGrid, ZonedDateTime.now())


        assertEquals(0, natal.pointsInHouse(HouseIndex.I).size)
        assertEquals(0, natal.pointsInHouse(HouseIndex.II).size)
        assertEquals(0, natal.pointsInHouse(HouseIndex.III).size)
        assertEquals(0, natal.pointsInHouse(HouseIndex.IV).size)
        assertEquals(0, natal.pointsInHouse(HouseIndex.V).size)
        assertEquals(1, natal.pointsInHouse(HouseIndex.VI).size)
        assertEquals(2, natal.pointsInHouse(HouseIndex.VII).size)
        assertEquals(1, natal.pointsInHouse(HouseIndex.VIII).size)
        assertEquals(0, natal.pointsInHouse(HouseIndex.IX).size)
        assertEquals(3, natal.pointsInHouse(HouseIndex.X).size)
        assertEquals(2, natal.pointsInHouse(HouseIndex.XI).size)
        assertEquals(1, natal.pointsInHouse(HouseIndex.XII).size)
    }
}