package com.bitbucket.anuarkaliyev23.hermes.core.points

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ApheticMarkTest {

    @Test
    fun compareTo() {
        assertTrue { ApheticMark.DETRIMENT < ApheticMark.FALL }
        assertTrue { ApheticMark.FALL < ApheticMark.FEUD }
        assertTrue { ApheticMark.FEUD < ApheticMark.NEUTRAL }
        assertTrue { ApheticMark.NEUTRAL < ApheticMark.KINSHIP }
        assertTrue { ApheticMark.KINSHIP < ApheticMark.EXALTATION }
        assertTrue { ApheticMark.EXALTATION < ApheticMark.DOMICILE }
    }

    @Test
    fun oppositeMark() {
        assertEquals(ApheticMark.oppositeMark(ApheticMark.DOMICILE), ApheticMark.DETRIMENT)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.EXALTATION), ApheticMark.FALL)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.KINSHIP), ApheticMark.FEUD)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.NEUTRAL), ApheticMark.NEUTRAL)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.FEUD), ApheticMark.KINSHIP)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.FALL), ApheticMark.EXALTATION)
        assertEquals(ApheticMark.oppositeMark(ApheticMark.DETRIMENT), ApheticMark.DOMICILE)
    }
}