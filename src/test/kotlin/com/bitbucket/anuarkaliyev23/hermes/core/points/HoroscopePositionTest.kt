package com.bitbucket.anuarkaliyev23.hermes.core.points

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class HoroscopePositionTest {
    @Test
    fun normalize() {
        assertEquals(0.0, HoroscopePosition.normalize(0.0))
        assertEquals(0.0, HoroscopePosition.normalize(360.0))
        assertEquals(30.0, HoroscopePosition.normalize(30.0))
        assertEquals(30.0, HoroscopePosition.normalize(390.0))
        assertEquals(30.0, HoroscopePosition.normalize(750.0))
        assertEquals(330.0, HoroscopePosition.normalize(-30.0))
        assertEquals(330.0, HoroscopePosition.normalize(-390.0))
        assertEquals(330.0, HoroscopePosition.normalize(-750.0))
    }

    @Test
    fun compareToOperator() {
        assertTrue { HoroscopePosition(0.0) == HoroscopePosition(0.0) }
        assertTrue { HoroscopePosition(180.0) == HoroscopePosition(180.0) }
        assertTrue { HoroscopePosition(0.0) == HoroscopePosition(360.0) }

        assertTrue { HoroscopePosition(0.0) < HoroscopePosition(1.0) }
        assertTrue { HoroscopePosition(0.0) < HoroscopePosition(180.0) }

        assertFalse{ HoroscopePosition(0.0) < HoroscopePosition(181.0) }

        assertTrue { HoroscopePosition(181.0) > HoroscopePosition(180.0) }
        assertTrue { HoroscopePosition(240.0) > HoroscopePosition(180.0) }
        assertTrue { HoroscopePosition(5.0) > HoroscopePosition(355.0) }

    }

    @Test
    fun aspect() {
        assertEquals(0.0, HoroscopePosition(0.0).aspect(HoroscopePosition(0.0)))
        assertEquals(5.0, HoroscopePosition(0.0).aspect(HoroscopePosition(5.0)))
        assertEquals(180.0, HoroscopePosition(0.0).aspect(HoroscopePosition(180.0)))
        assertEquals(180.0, HoroscopePosition(30.0).aspect(HoroscopePosition(210.0)))
        assertEquals(179.0, HoroscopePosition(0.0).aspect(HoroscopePosition(181.0)))
        assertEquals(10.0, HoroscopePosition(5.0).aspect(HoroscopePosition(355.0)))
    }


    @Test
    fun of() {
        assertEquals(0.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.ARIES, 0))
        assertEquals(15.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.ARIES, 0))

        assertEquals(30.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.TAURUS, 0))
        assertEquals(45.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.TAURUS, 0))

        assertEquals(60.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.GEMINI, 0))
        assertEquals(75.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.GEMINI, 0))

        assertEquals(90.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.CANCER, 0))
        assertEquals(105.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.CANCER, 0))

        assertEquals(120.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.LEO, 0))
        assertEquals(135.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.LEO, 0))

        assertEquals(150.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.VIRGO, 0))
        assertEquals(165.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.VIRGO, 0))

        assertEquals(180.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.LIBRA, 0))
        assertEquals(195.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.LIBRA, 0))

        assertEquals(210.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.SCORPIO, 0))
        assertEquals(225.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.SCORPIO, 0))

        assertEquals(240.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.SAGITTARIUS, 0))
        assertEquals(255.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.SAGITTARIUS, 0))

        assertEquals(270.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.CAPRICORN, 0))
        assertEquals(285.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.CAPRICORN, 0))

        assertEquals(300.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.AQUARIUS, 0))
        assertEquals(315.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.AQUARIUS, 0))

        assertEquals(330.0.toHoroscopePosition(), HoroscopePosition.of(0, Sign.PISCES, 0))
        assertEquals(345.0.toHoroscopePosition(), HoroscopePosition.of(15, Sign.PISCES, 0))




    }

    @Test
    fun degrees() {
        val position = 42.3601.toHoroscopePosition()
        assertEquals(42, position.degrees())

    }

    @Test
    fun minutes() {
        val position = 42.3601.toHoroscopePosition()
        assertEquals(21, position.minutes())
    }

    @Test
    fun seconds() {
        val position = 42.3601.toHoroscopePosition()
        assertEquals(36, position.seconds())
    }


    @Test
    fun sign() {
        assertEquals(Sign.ARIES, 0.0.toHoroscopePosition().sign())
        assertEquals(Sign.ARIES, 15.0.toHoroscopePosition().sign())
        assertEquals(Sign.TAURUS, 30.0.toHoroscopePosition().sign())
        assertEquals(Sign.TAURUS, 45.0.toHoroscopePosition().sign())
        assertEquals(Sign.GEMINI, 60.0.toHoroscopePosition().sign())
        assertEquals(Sign.GEMINI, 75.0.toHoroscopePosition().sign())
        assertEquals(Sign.CANCER, 90.0.toHoroscopePosition().sign())
        assertEquals(Sign.CANCER, 105.0.toHoroscopePosition().sign())
        assertEquals(Sign.LEO, 120.0.toHoroscopePosition().sign())
        assertEquals(Sign.LEO, 135.0.toHoroscopePosition().sign())
        assertEquals(Sign.VIRGO, 150.0.toHoroscopePosition().sign())
        assertEquals(Sign.VIRGO, 175.0.toHoroscopePosition().sign())
        assertEquals(Sign.LIBRA, 180.0.toHoroscopePosition().sign())
        assertEquals(Sign.LIBRA, 195.0.toHoroscopePosition().sign())
        assertEquals(Sign.SCORPIO, 210.0.toHoroscopePosition().sign())
        assertEquals(Sign.SCORPIO, 225.0.toHoroscopePosition().sign())
        assertEquals(Sign.SAGITTARIUS, 240.0.toHoroscopePosition().sign())
        assertEquals(Sign.SAGITTARIUS, 255.0.toHoroscopePosition().sign())
        assertEquals(Sign.CAPRICORN, 270.0.toHoroscopePosition().sign())
        assertEquals(Sign.CAPRICORN, 285.0.toHoroscopePosition().sign())
        assertEquals(Sign.AQUARIUS, 300.0.toHoroscopePosition().sign())
        assertEquals(Sign.AQUARIUS, 315.0.toHoroscopePosition().sign())
        assertEquals(Sign.PISCES, 330.0.toHoroscopePosition().sign())
        assertEquals(Sign.PISCES, 345.0.toHoroscopePosition().sign())
    }
}