package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class HouseTest {


    @Test
    fun compareTo() {
        val house1 = House(HouseIndex.I, HoroscopePosition(0.0))
        val house2 = House(HouseIndex.II, HoroscopePosition(30.0))
        val house3 = House(HouseIndex.III, HoroscopePosition(60.0))
        val house4 = House(HouseIndex.IV, HoroscopePosition(90.0))
        val house5 = House(HouseIndex.V, HoroscopePosition(120.0))
        val house6 = House(HouseIndex.VI, HoroscopePosition(150.0))
        val house7 = House(HouseIndex.VII, HoroscopePosition(180.0))
        val house8 = House(HouseIndex.VIII, HoroscopePosition(210.0))
        val house9 = House(HouseIndex.IX, HoroscopePosition(240.0))
        val house10 = House(HouseIndex.X, HoroscopePosition(270.0))
        val house11 = House(HouseIndex.XI, HoroscopePosition(300.0))
        val house12 = House(HouseIndex.XII, HoroscopePosition(330.0))

        assertTrue(house2 > house1)
        assertTrue(house3 > house2)
        assertTrue(house4 > house3)
        assertTrue(house5 > house4)
        assertTrue(house6 > house5)
        assertTrue(house7 > house6)
        assertTrue(house8 > house7)
        assertTrue(house9 > house8)
        assertTrue(house10 > house9)
        assertTrue(house11 > house10)
        assertTrue(house12 > house11)
    }
}