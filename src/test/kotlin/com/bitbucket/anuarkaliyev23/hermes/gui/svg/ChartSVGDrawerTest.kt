package com.bitbucket.anuarkaliyev23.hermes.gui.svg

import com.bitbucket.anuarkaliyev23.hermes.core.aspects.PlanetBasedOrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.HermesSwissEphemerisWrapper
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.JnaSwissEphemerisLibraryFactory
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisChartFactory
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibraryFactory
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate
import java.time.ZonedDateTime
import java.util.*
import kotlin.collections.HashMap

internal class ChartSVGDrawerTest {

    @Test
    fun wrappedSVG() {
        val libraryFactory = JnaSwissEphemerisLibraryFactory("sweph\\swedll64.dll")
        val wrapper = HermesSwissEphemerisWrapper(libraryFactory)
        val chartFactory = SwissEphemerisChartFactory(wrapper)

        val svgDrawer = ChartSVGDrawer(120.0, 100.0, 80.0, 50.0, 150.0, 160.0)

        val planetMap = HashMap<PlanetType, Double>()
        planetMap[PlanetType.SUN] = 5.0
        planetMap[PlanetType.MOON] = 5.0
        planetMap[PlanetType.MERCURY] = 1.0
        planetMap[PlanetType.VENUS] = 1.0
        planetMap[PlanetType.MARS] = 1.0
        planetMap[PlanetType.JUPITER] = 3.0
        planetMap[PlanetType.SATURN] = 3.0
        planetMap[PlanetType.URANUS] = 1.0
        planetMap[PlanetType.NEPTUNE] = 1.0
        planetMap[PlanetType.PLUTO] = 1.0

        val orb = PlanetBasedOrbTable(planetMap)
        val svg = svgDrawer.wrappedSVG(chartFactory.houseChart(ZonedDateTime.now(), 43.15, 76.57, HouseSystem.PLACIDUS), orb)
        println(svg)
    }

}