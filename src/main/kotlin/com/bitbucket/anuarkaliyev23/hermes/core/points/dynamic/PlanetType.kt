package com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic

import com.bitbucket.anuarkaliyev23.hermes.core.points.ApheticMark
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign

enum class PlanetType(val averageSpeed : Double, val unicodeSymbol: String)  {
    SUN     (PlanetType.SUN_AVERAGE_SPEED, "\u2609"),
    MOON    (PlanetType.MOON_AVERAGE_SPEED, "\u263d"),
    MERCURY (PlanetType.MERCURY_AVERAGE_SPEED, "\u263f"),
    VENUS   (PlanetType.VENUS_AVERAGE_SPEED, "\u2640"),
    MARS    (PlanetType.MARS_AVERAGE_SPEED, "\u2642"),
    JUPITER (PlanetType.JUPITER_AVERAGE_SPEED, "\u2643"),
    SATURN  (PlanetType.SATURN_AVERAGE_SPEED, "\u2644"),
    URANUS  (PlanetType.URANUS_AVERAGE_SPEED, "\u2645"),
    NEPTUNE (PlanetType.NEPTUNE_AVERAGE_SPEED, "\u2646"),
    PLUTO   (PlanetType.PLUTO_AVERAGE_SPEED, "\u2647");

    fun apheticMap() : Map<Sign, ApheticMark> = apheticMapOf(this)

    companion object {
        private const val SUN_AVERAGE_SPEED = 0.98573
        private const val MOON_AVERAGE_SPEED = 13.17552
        private const val MERCURY_AVERAGE_SPEED = 1.21707
        private const val VENUS_AVERAGE_SPEED = 1.0408
        private const val MARS_AVERAGE_SPEED = 0.56614
        private const val JUPITER_AVERAGE_SPEED = 0.13889
        private const val SATURN_AVERAGE_SPEED = 0.0695
        private const val URANUS_AVERAGE_SPEED = 0.03351
        private const val NEPTUNE_AVERAGE_SPEED = 0.02133
        private const val PLUTO_AVERAGE_SPEED = 0.01588


        private val SUN_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.EXALTATION,
                Sign.TAURUS      to     ApheticMark.NEUTRAL,
                Sign.GEMINI      to     ApheticMark.FEUD,
                Sign.CANCER      to     ApheticMark.NEUTRAL,
                Sign.LEO         to     ApheticMark.DOMICILE,
                Sign.VIRGO       to     ApheticMark.NEUTRAL,
                Sign.LIBRA       to     ApheticMark.FALL,
                Sign.SCORPIO     to     ApheticMark.NEUTRAL,
                Sign.SAGITTARIUS to     ApheticMark.KINSHIP,
                Sign.CAPRICORN   to     ApheticMark.NEUTRAL,
                Sign.AQUARIUS    to     ApheticMark.DETRIMENT,
                Sign.PISCES      to     ApheticMark.NEUTRAL
        )

        private val MOON_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.NEUTRAL,
                Sign.TAURUS      to     ApheticMark.EXALTATION,
                Sign.GEMINI      to     ApheticMark.NEUTRAL,
                Sign.CANCER      to     ApheticMark.DOMICILE,
                Sign.LEO         to     ApheticMark.NEUTRAL,
                Sign.VIRGO       to     ApheticMark.FEUD,
                Sign.LIBRA       to     ApheticMark.NEUTRAL,
                Sign.SCORPIO     to     ApheticMark.FALL,
                Sign.SAGITTARIUS to     ApheticMark.NEUTRAL,
                Sign.CAPRICORN   to     ApheticMark.DETRIMENT,
                Sign.AQUARIUS    to     ApheticMark.NEUTRAL,
                Sign.PISCES      to     ApheticMark.KINSHIP
        )

        private val MERCURY_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.FEUD,
                Sign.TAURUS      to     ApheticMark.KINSHIP,
                Sign.GEMINI      to     ApheticMark.DOMICILE,
                Sign.CANCER      to     ApheticMark.FEUD,
                Sign.LEO         to     ApheticMark.FEUD,
                Sign.VIRGO       to     ApheticMark.DOMICILE,
                Sign.LIBRA       to     ApheticMark.KINSHIP,
                Sign.SCORPIO     to     ApheticMark.FEUD,
                Sign.SAGITTARIUS to     ApheticMark.DETRIMENT,
                Sign.CAPRICORN   to     ApheticMark.KINSHIP,
                Sign.AQUARIUS    to     ApheticMark.KINSHIP,
                Sign.PISCES      to     ApheticMark.DETRIMENT
        )

        private val VENUS_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.DETRIMENT,
                Sign.TAURUS      to     ApheticMark.DOMICILE,
                Sign.GEMINI      to     ApheticMark.KINSHIP,
                Sign.CANCER      to     ApheticMark.FEUD,
                Sign.LEO         to     ApheticMark.FEUD,
                Sign.VIRGO       to     ApheticMark.FALL,
                Sign.LIBRA       to     ApheticMark.DOMICILE,
                Sign.SCORPIO     to     ApheticMark.DETRIMENT,
                Sign.SAGITTARIUS to     ApheticMark.FEUD,
                Sign.CAPRICORN   to     ApheticMark.KINSHIP,
                Sign.AQUARIUS    to     ApheticMark.KINSHIP,
                Sign.PISCES      to     ApheticMark.EXALTATION
        )

        private val MARS_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.DOMICILE,
                Sign.TAURUS      to     ApheticMark.DETRIMENT,
                Sign.GEMINI      to     ApheticMark.FEUD,
                Sign.CANCER      to     ApheticMark.FALL,
                Sign.LEO         to     ApheticMark.KINSHIP,
                Sign.VIRGO       to     ApheticMark.FEUD,
                Sign.LIBRA       to     ApheticMark.DETRIMENT,
                Sign.SCORPIO     to     ApheticMark.DOMICILE,
                Sign.SAGITTARIUS to     ApheticMark.KINSHIP,
                Sign.CAPRICORN   to     ApheticMark.EXALTATION,
                Sign.AQUARIUS    to     ApheticMark.FEUD,
                Sign.PISCES      to     ApheticMark.KINSHIP
        )

        private val JUPITER_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.KINSHIP,
                Sign.TAURUS      to     ApheticMark.FEUD,
                Sign.GEMINI      to     ApheticMark.DETRIMENT,
                Sign.CANCER      to     ApheticMark.EXALTATION,
                Sign.LEO         to     ApheticMark.KINSHIP,
                Sign.VIRGO       to     ApheticMark.DETRIMENT,
                Sign.LIBRA       to     ApheticMark.FEUD,
                Sign.SCORPIO     to     ApheticMark.KINSHIP,
                Sign.SAGITTARIUS to     ApheticMark.DOMICILE,
                Sign.CAPRICORN   to     ApheticMark.FALL,
                Sign.AQUARIUS    to     ApheticMark.FEUD,
                Sign.PISCES      to     ApheticMark.DOMICILE
        )
        private val SATURN_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.FALL,
                Sign.TAURUS      to     ApheticMark.KINSHIP,
                Sign.GEMINI      to     ApheticMark.KINSHIP,
                Sign.CANCER      to     ApheticMark.DETRIMENT,
                Sign.LEO         to     ApheticMark.DETRIMENT,
                Sign.VIRGO       to     ApheticMark.KINSHIP,
                Sign.LIBRA       to     ApheticMark.EXALTATION,
                Sign.SCORPIO     to     ApheticMark.FEUD,
                Sign.SAGITTARIUS to     ApheticMark.FEUD,
                Sign.CAPRICORN   to     ApheticMark.DOMICILE,
                Sign.AQUARIUS    to     ApheticMark.DOMICILE,
                Sign.PISCES      to     ApheticMark.FEUD
        )
        private val URANUS_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.FEUD,
                Sign.TAURUS      to     ApheticMark.FALL,
                Sign.GEMINI      to     ApheticMark.KINSHIP,
                Sign.CANCER      to     ApheticMark.DETRIMENT,
                Sign.LEO         to     ApheticMark.DETRIMENT,
                Sign.VIRGO       to     ApheticMark.KINSHIP,
                Sign.LIBRA       to     ApheticMark.KINSHIP,
                Sign.SCORPIO     to     ApheticMark.EXALTATION,
                Sign.SAGITTARIUS to     ApheticMark.FEUD,
                Sign.CAPRICORN   to     ApheticMark.DOMICILE,
                Sign.AQUARIUS    to     ApheticMark.DOMICILE,
                Sign.PISCES      to     ApheticMark.FEUD
        )
        private val NEPTUNE_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.KINSHIP,
                Sign.TAURUS      to     ApheticMark.FEUD,
                Sign.GEMINI      to     ApheticMark.DETRIMENT,
                Sign.CANCER      to     ApheticMark.KINSHIP,
                Sign.LEO         to     ApheticMark.FALL,
                Sign.VIRGO       to     ApheticMark.DETRIMENT,
                Sign.LIBRA       to     ApheticMark.FEUD,
                Sign.SCORPIO     to     ApheticMark.KINSHIP,
                Sign.SAGITTARIUS to     ApheticMark.DOMICILE,
                Sign.CAPRICORN   to     ApheticMark.FEUD,
                Sign.AQUARIUS    to     ApheticMark.EXALTATION,
                Sign.PISCES      to     ApheticMark.DOMICILE
        )
        private val PLUTO_APHETIC_MAP = mapOf(
                Sign.ARIES       to     ApheticMark.DOMICILE,
                Sign.TAURUS      to     ApheticMark.DETRIMENT,
                Sign.GEMINI      to     ApheticMark.FEUD,
                Sign.CANCER      to     ApheticMark.KINSHIP,
                Sign.LEO         to     ApheticMark.EXALTATION,
                Sign.VIRGO       to     ApheticMark.FEUD,
                Sign.LIBRA       to     ApheticMark.DETRIMENT,
                Sign.SCORPIO     to     ApheticMark.DOMICILE,
                Sign.SAGITTARIUS to     ApheticMark.KINSHIP,
                Sign.CAPRICORN   to     ApheticMark.FEUD,
                Sign.AQUARIUS    to     ApheticMark.FALL,
                Sign.PISCES      to     ApheticMark.KINSHIP
        )

        fun apheticMapOf(planet : PlanetType) : Map<Sign, ApheticMark> {
            return when(planet) {
                PlanetType.SUN -> SUN_APHETIC_MAP
                PlanetType.MOON -> MOON_APHETIC_MAP
                PlanetType.MERCURY -> MERCURY_APHETIC_MAP
                PlanetType.VENUS -> VENUS_APHETIC_MAP
                PlanetType.MARS -> MARS_APHETIC_MAP
                PlanetType.JUPITER -> JUPITER_APHETIC_MAP
                PlanetType.SATURN -> SATURN_APHETIC_MAP
                PlanetType.URANUS -> URANUS_APHETIC_MAP
                PlanetType.NEPTUNE -> NEPTUNE_APHETIC_MAP
                PlanetType.PLUTO -> PLUTO_APHETIC_MAP
            }
        }

    }
}