package com.bitbucket.anuarkaliyev23.hermes.core.aspects

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.DynamicHoroscopePoint
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType

class Aspect(val first: HoroscopePoint, val second: HoroscopePoint, val orb: Double = 0.0) {
    val value: Double = first - second
    val isApplicational: Boolean? = checkApplicational()

    fun type() : AspectType? {
        for (type in AspectType.values()) {
            if (value in (type.degree - orb)..(type.degree + orb))
                return type
        }
        return null
    }

    fun points() : List<HoroscopePoint> = arrayListOf(first, second)
    fun contains(planetType: PlanetType) : Boolean {
        return this.points().filterIsInstance<Planet>().any { it.type == planetType }
    }

    fun hasType() : Boolean = type() != null

    private fun checkApplicational() : Boolean? {
        if (this.type() == null) return null
        if (first !is DynamicHoroscopePoint || second !is DynamicHoroscopePoint) return null

        val maxSpeed = DynamicHoroscopePoint.maxSpeed(first, second)
        val minSpeed = DynamicHoroscopePoint.minSpeed(first, second)
        val diff = maxSpeed.position - minSpeed.position

        return if (maxSpeed.isDirect()) {
            diff <= this.type()!!.degree
        } else {
            diff > this.type()!!.degree
        }
    }



    private fun samePointsWith(other: Aspect): Boolean {
        return (this.first == other.first && this.second == other.second) || (this.second == other.first && this.first == other.second)
    }

    override fun toString(): String {
        return "Aspect(type=${this.type()}, isApplicational=${this.isApplicational}, first=$first, second=$second, orb=$orb, value=$value)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Aspect
        if (!samePointsWith(other)) return false
        if (orb != other.orb) return false
        if (value != other.value) return false
        if (isApplicational != other.isApplicational) return false

        return true
    }

    override fun hashCode(): Int {
        var result = orb.hashCode()
        result = 31 * result + value.hashCode()
        result = 31 * result + (isApplicational?.hashCode() ?: 0)
        return result
    }
}