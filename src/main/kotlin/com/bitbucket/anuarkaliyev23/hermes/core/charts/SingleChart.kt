package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.aspects.Aspect
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.OrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet


interface SingleChart {
    fun aspects(orbTable: OrbTable) : Set<Aspect>
    val planets: List<Planet>
}