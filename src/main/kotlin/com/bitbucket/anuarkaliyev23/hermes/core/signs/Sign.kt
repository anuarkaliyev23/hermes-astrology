package com.bitbucket.anuarkaliyev23.hermes.core.signs

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition


enum class Sign(val quality: Quality, val element: Element, val startPoint : Int, val startPosition: HoroscopePosition, val symbol: String) {
    ARIES       (Quality.CARDINAL,  Element.FIRE,   Sign.ARIES_START_POINT,         Sign.ARIES_START_POINT.toHoroscopePosition(),        "\u2648"),
    TAURUS      (Quality.FIXED,     Element.EARTH,  Sign.TAURUS_START_POINT,        Sign.TAURUS_START_POINT.toHoroscopePosition(),       "\u2649"),
    GEMINI      (Quality.MUTABLE,   Element.AIR,    Sign.GEMINI_START_POINT,        Sign.GEMINI_START_POINT.toHoroscopePosition(),       "\u264a"),
    CANCER      (Quality.CARDINAL,  Element.WATER,  Sign.CANCER_START_POINT,        Sign.CANCER_START_POINT.toHoroscopePosition(),       "\u264b"),
    LEO         (Quality.FIXED,     Element.FIRE,   Sign.LEO_START_POINT,           Sign.LEO_START_POINT.toHoroscopePosition(),          "\u264c"),
    VIRGO       (Quality.MUTABLE,   Element.EARTH,  Sign.VIRGO_START_POINT,         Sign.VIRGO_START_POINT.toHoroscopePosition(),        "\u264d"),
    LIBRA       (Quality.CARDINAL,  Element.AIR,    Sign.LIBRA_START_POINT,         Sign.LIBRA_START_POINT.toHoroscopePosition(),        "\u264e"),
    SCORPIO     (Quality.FIXED,     Element.WATER,  Sign.SCORPIO_START_POINT,       Sign.SCORPIO_START_POINT.toHoroscopePosition(),      "\u264f"),
    SAGITTARIUS (Quality.MUTABLE,   Element.FIRE,   Sign.SAGITTARIUS_START_POINT,   Sign.SAGITTARIUS_START_POINT.toHoroscopePosition(),  "\u2650"),
    CAPRICORN   (Quality.CARDINAL,  Element.EARTH,  Sign.CAPRICORN_START_POINT,     Sign.CAPRICORN_START_POINT.toHoroscopePosition(),    "\u2651"),
    AQUARIUS    (Quality.FIXED,     Element.AIR,    Sign.AQUARIUS_START_POINT,      Sign.AQUARIUS_START_POINT.toHoroscopePosition(),     "\u2652"),
    PISCES      (Quality.MUTABLE,   Element.WATER,  Sign.PISCES_START_POINT,        Sign.PISCES_START_POINT.toHoroscopePosition(),       "\u2653");

    fun nextSign() = Sign.nextSign(this)
    fun prevSign() = Sign.prevSign(this)
    fun oppositeSign() = Sign.oppositeSign(this)

    companion object {
        private const val ARIES_START_POINT = 0
        private const val TAURUS_START_POINT = 30
        private const val GEMINI_START_POINT = 60
        private const val CANCER_START_POINT = 90
        private const val LEO_START_POINT = 120
        private const val VIRGO_START_POINT = 150
        private const val LIBRA_START_POINT = 180
        private const val SCORPIO_START_POINT = 210
        private const val SAGITTARIUS_START_POINT = 240
        private const val CAPRICORN_START_POINT = 270
        private const val AQUARIUS_START_POINT = 300
        private const val PISCES_START_POINT = 330


        fun nextSign(sign : Sign) : Sign {
            return when (sign) {
                PISCES -> ARIES
                else -> Sign.values()[sign.ordinal + 1]
            }
        }

        fun prevSign(sign : Sign) : Sign {
            return when (sign) {
                ARIES -> PISCES
                else -> Sign.values()[sign.ordinal - 1]
            }
        }

        fun of(position: Double) : Sign {
            return of(HoroscopePosition(position))
        }

        fun of(position: HoroscopePosition) : Sign {
            Sign.values().sortedDescending().forEach {
                if (position.coordinate >= it.startPoint) return it
            }
            throw com.bitbucket.anuarkaliyev23.hermes.core.exceptions.UnrecognizedSignException()
        }

        fun oppositeSign(sign: Sign) : Sign = Sign.values()[(sign.ordinal + (Sign.values().size / 2)) % Sign.values().size]

    }
}