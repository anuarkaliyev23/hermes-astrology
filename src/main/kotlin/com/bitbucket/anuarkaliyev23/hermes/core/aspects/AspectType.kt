package com.bitbucket.anuarkaliyev23.hermes.core.aspects

enum class AspectType(val degree : Double, var isMajor : Boolean, val quality: AspectQuality) {

    CONJUNCTION(AspectType.CONJUNCTION_DEGREE, true, AspectQuality.UNDETERMINED),
    OPPOSITION (AspectType.OPPOSITION_DEGREE,  true, AspectQuality.STRAINING),
    TRINE      (AspectType.TRINE_DEGREE,       true, AspectQuality.HARMONIOUS),
    SQUARE     (AspectType.SQUARE_DEGREE,      true, AspectQuality.STRAINING),
    QUINTILE   (AspectType.QUINTILE_DEGREE,    true, AspectQuality.CREATIVE),
    SEXTILE    (AspectType.SEXTILE_DEGREE,     true, AspectQuality.HARMONIOUS),
    SEMISQUARE (AspectType.SEMISQUARE_DEGREE,  true, AspectQuality.STRAINING),
    SEMISEXTILE(AspectType.SEMISEXTILE_DEGREE, true, AspectQuality.HARMONIOUS);

    companion object {
        const val CONJUNCTION_DEGREE = 0.0
        const val OPPOSITION_DEGREE = 180.0
        const val TRINE_DEGREE = 120.0
        const val SQUARE_DEGREE = 90.0
        const val QUINTILE_DEGREE = 72.0
        const val SEXTILE_DEGREE = 60.0
        const val SEMISQUARE_DEGREE = 45.0
        const val SEMISEXTILE_DEGREE = 40.0
    }
}