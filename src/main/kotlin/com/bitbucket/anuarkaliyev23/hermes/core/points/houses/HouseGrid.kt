package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.House
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import java.util.*


interface HouseGrid {
    val houses: SortedSet<House>
    val system: HouseSystem
    operator fun get(index: HouseIndex) : House
    fun houseOf(point: HoroscopePoint) : House
}