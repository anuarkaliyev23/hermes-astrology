package com.bitbucket.anuarkaliyev23.hermes.core.signs

enum class Element {
    FIRE, EARTH, AIR, WATER;
}