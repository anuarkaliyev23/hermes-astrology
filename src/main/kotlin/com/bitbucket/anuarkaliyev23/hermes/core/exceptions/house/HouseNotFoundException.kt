package com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house

class HouseNotFoundException : GeneralHouseException()