package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house.HouseNotFoundException
import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house.IncompleteHouseGridException
import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import java.util.*

class SimpleHouseGrid(
        override val houses: SortedSet<House>,
        override val system: HouseSystem = HouseSystem.PLACIDUS
) : HouseGrid {
    init {
        if (houses.size != HouseIndex.values().size) throw IncompleteHouseGridException()
        if (houses.map { it.index }.toSet().size != HouseIndex.values().size) throw IncompleteHouseGridException()
    }

    override fun houseOf(point: HoroscopePoint): House {
        houses.forEach {house ->
            val nextHouse = this[house.index.nextIndex()]
            if (house.position <= point.position && nextHouse.position > point.position)
                return house
        }
        throw HouseNotFoundException()
    }

    override fun get(index: HouseIndex): House {
        return houses.find { it.index == index } ?: throw HouseNotFoundException()
    }

    override fun toString(): String {
        return "SimpleHouseGrid(houses=$houses, system=$system)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SimpleHouseGrid

        if (houses != other.houses) return false
        if (system != other.system) return false

        return true
    }

    override fun hashCode(): Int {
        var result = houses.hashCode()
        result = 31 * result + system.hashCode()
        return result
    }


}