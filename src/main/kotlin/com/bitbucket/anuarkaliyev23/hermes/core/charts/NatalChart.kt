package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseGrid
import java.time.ZonedDateTime

class NatalChart(
        override val planets: List<Planet>,
        override val houseGrid: HouseGrid,
        override var date: ZonedDateTime? = null
) : Cosmogram(planets, date), HouseChart {
    override fun pointsInHouse(index: HouseIndex): List<Planet> {
        return planets.filter { planet -> houseGrid.houseOf(planet).index == index }.toList()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as NatalChart

        if (planets != other.planets) return false
        if (houseGrid != other.houseGrid) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + planets.hashCode()
        result = 31 * result + houseGrid.hashCode()
        return result
    }
}