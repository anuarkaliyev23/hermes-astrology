package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.aspects.Aspect
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.AspectBasedOrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.OrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.PlanetBasedOrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import java.time.ZonedDateTime

open class Cosmogram(
        override val planets: List<Planet>,
        open var date: ZonedDateTime? = null
) : SingleChart {
    override fun aspects(orbTable: OrbTable): Set<Aspect> {
        when (orbTable) {
            is PlanetBasedOrbTable -> return handlePlanetBaseOrbTable(orbTable)
            is AspectBasedOrbTable -> throw NotImplementedError()
        }
    }
    private fun handlePlanetBaseOrbTable(orbTable: PlanetBasedOrbTable): Set<Aspect> {
        val aspects = HashSet<Aspect>()

        planets.forEach{planet ->
            planets.filter { it != planet }.forEach { secondPlanet ->
                val maxOrb = Math.max(orbTable[planet.type], orbTable[secondPlanet.type])
                val aspect = Aspect(planet, secondPlanet, orb = maxOrb)
                aspects.add(aspect)
            }
        }
        return aspects
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Cosmogram

        if (planets != other.planets) return false

        return true
    }

    override fun hashCode(): Int {
        return planets.hashCode()
    }

    override fun toString(): String {
        return "Cosmogram(planets=$planets, date=$date)"
    }


}