package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

enum class HouseIndex(val quality: HouseQuality, val index : Int) {
    I   (HouseQuality.ANGULAR,      1),
    II  (HouseQuality.SUCCEDENT,    2),
    III (HouseQuality.CADENT,       3),
    IV  (HouseQuality.ANGULAR,      4),
    V   (HouseQuality.SUCCEDENT,    5),
    VI  (HouseQuality.CADENT,       6),
    VII (HouseQuality.ANGULAR,      7),
    VIII(HouseQuality.SUCCEDENT,    8),
    IX  (HouseQuality.CADENT,       9),
    X   (HouseQuality.ANGULAR,      10),
    XI  (HouseQuality.SUCCEDENT,    11),
    XII (HouseQuality.CADENT,       12);

    fun oppositeIndex() = oppositeIndex(this)
    fun nextIndex() : HouseIndex {
        return if (this == XII) I
        else values()[this.ordinal + 1]
    }
    companion object {
        fun oppositeIndex(index: HouseIndex) : HouseIndex =  values()[(index.ordinal + (values().size / 2)) % values().size]
    }
}