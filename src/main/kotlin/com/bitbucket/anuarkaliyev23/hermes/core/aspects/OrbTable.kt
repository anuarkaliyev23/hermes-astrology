package com.bitbucket.anuarkaliyev23.hermes.core.aspects

import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.aspect.IncompleteOrbTableException
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import java.util.*
import kotlin.collections.HashMap

sealed class OrbTable

class PlanetBasedOrbTable(
        private val planetToOrbMap: Map<PlanetType, Double> = EnumMap(PlanetType::class.java)
) : com.bitbucket.anuarkaliyev23.hermes.core.aspects.OrbTable() {
    init {
        if (planetToOrbMap.keys.size != PlanetType.values().size) throw IncompleteOrbTableException()
    }

    operator fun get(planetType: PlanetType): Double  {
        return planetToOrbMap[planetType] ?: throw IncompleteOrbTableException()
    }
}

class AspectBasedOrbTable(
        private val aspectToOrbMap: Map<AspectType, Double> = EnumMap(AspectType::class.java)
) : com.bitbucket.anuarkaliyev23.hermes.core.aspects.OrbTable() {
    init {
        if (aspectToOrbMap.keys.size != AspectType.values().size) throw IncompleteOrbTableException()
    }

    operator fun get(aspectType: AspectType): Double {
        return aspectToOrbMap[aspectType] ?: throw IncompleteOrbTableException()
    }
}