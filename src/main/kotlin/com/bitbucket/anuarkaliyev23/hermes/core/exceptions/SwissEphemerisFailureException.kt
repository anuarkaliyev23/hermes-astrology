package com.bitbucket.anuarkaliyev23.hermes.core.exceptions

class SwissEphemerisFailureException(private val ephemerisErrorText: String) : GeneralHermesException() {
    override val message: String?
        get() = "Swiss Ephemeris failed with message = [$ephemerisErrorText]"
}