package com.bitbucket.anuarkaliyev23.hermes.core.extensions

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime

fun Double.toHoroscopePosition(): HoroscopePosition {
    return HoroscopePosition(this)
}

fun Int.toHoroscopePosition() : HoroscopePosition {
    return this.toDouble().toHoroscopePosition()
}

fun ZonedDateTime.toLocalDateTimeUTC(): LocalDateTime = this.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()

fun LocalDateTime.toZonedDateTimeUTC(): ZonedDateTime = ZonedDateTime.of(this, ZoneOffset.UTC)

fun Any.logger(): Logger = LoggerFactory.getLogger(this::class.java)