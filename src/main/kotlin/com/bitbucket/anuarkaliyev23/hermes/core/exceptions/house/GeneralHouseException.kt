package com.bitbucket.anuarkaliyev23.hermes.core.exceptions.house

import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.GeneralHermesException

open class GeneralHouseException : GeneralHermesException()