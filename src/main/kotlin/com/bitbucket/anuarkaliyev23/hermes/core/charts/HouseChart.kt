package com.bitbucket.anuarkaliyev23.hermes.core.charts

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseGrid
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex

interface HouseChart: SingleChart {
    val houseGrid: HouseGrid
    fun pointsInHouse(index: HouseIndex) : List<HoroscopePoint>
}