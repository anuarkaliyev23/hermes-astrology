package com.bitbucket.anuarkaliyev23.hermes.core.points

import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign

class HoroscopePosition(coordinate: Double) : Comparable<HoroscopePosition> {
    var coordinate: Double = normalize(coordinate)
        set(value) { field = normalize(value) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HoroscopePosition

        if (coordinate != other.coordinate) return false

        return true
    }

    operator fun minus(other: HoroscopePosition): Double {
        return this.coordinate - other.coordinate
    }

    override fun hashCode(): Int {
        return coordinate.hashCode()
    }

    override fun toString(): String {
        return "HoroscopePosition(coordinate=$coordinate)"
    }

    fun degrees() : Int {
        return coordinate.toInt()
    }

    fun minutes() : Int {
        return ((coordinate - degrees()) * MINUTES_IN_DEGREES).toInt()
    }

    fun seconds() : Int {
        val secondsDecimal = (((coordinate - degrees()) * MINUTES_IN_DEGREES)) - minutes()
        return (secondsDecimal * MINUTES_IN_DEGREES).toInt()
    }

    fun sign() : Sign = Sign.of(this)

    override operator fun compareTo(other: HoroscopePosition): Int {
        return when {
            this.coordinate == other.coordinate -> 0
            //if on the same side
            this.coordinate >= HALF_MAX_POSITION && other.coordinate >= HALF_MAX_POSITION
                    || this.coordinate < HALF_MAX_POSITION && other.coordinate < HALF_MAX_POSITION -> if (this.coordinate  > other.coordinate) 1 else -1
            //if one is on the lower side and other on higher side
            this.coordinate < HALF_MAX_POSITION && other.coordinate >= HALF_MAX_POSITION -> if (this.coordinate + HALF_MAX_POSITION < other.coordinate) 1 else -1
            else -> if (normalize(this.coordinate + HALF_MAX_POSITION) < other.coordinate) 1 else -1
        }
    }

    fun aspect(other: HoroscopePosition): Double {
        val max = Math.max(this.coordinate, other.coordinate)
        val min = Math.min(this.coordinate, other.coordinate)
        val result = max - min

        return if (result > 180) MAX_POSITION - result else result
    }


    companion object {
        const val MAX_POSITION = 360
        const val HALF_MAX_POSITION = 180
        const val MINUTES_IN_DEGREES = 60

        fun normalize(position : Double) : Double = if (position >= 0) (position % MAX_POSITION) else (MAX_POSITION + (position % MAX_POSITION)) % MAX_POSITION

        fun degreeToDecimalDegree(degree: Int, minute: Int): Double {
            val decimalMinute = minute.toDouble() / MINUTES_IN_DEGREES
            return degree + decimalMinute
        }

        fun of(degree: Int, sign: Sign, minute: Int): HoroscopePosition {
            val decimal = degreeToDecimalDegree(degree + sign.startPosition.coordinate.toInt(), minute)
            return HoroscopePosition(decimal)
        }
    }

}