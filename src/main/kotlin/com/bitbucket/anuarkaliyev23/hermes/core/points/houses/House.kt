package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition

data class House(
        var index : HouseIndex,
        override var position: HoroscopePosition
) : HoroscopePoint, Comparable<House> {

    override operator fun compareTo(other: House) : Int {
        return this.index.compareTo(other.index)
    }
}