package com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePosition
import kotlin.math.abs

data class Planet(
        var type: PlanetType,
        override var position: HoroscopePosition,
        override var speed: Double
) : DynamicHoroscopePoint {
    fun speedToAverageRatio(): Double = abs(speed / type.averageSpeed)
}