package com.bitbucket.anuarkaliyev23.hermes.core.points

enum class ApheticMark(val point : Int) {
    DETRIMENT   (ApheticMark.DETRIMENT_POINT),
    FALL        (ApheticMark.FALL_POINT),
    FEUD        (ApheticMark.FEUD_POINT),
    NEUTRAL     (ApheticMark.NEUTRAL_POINT),
    KINSHIP     (ApheticMark.KINSHIP_POINT),
    EXALTATION  (ApheticMark.EXALTATION_POINT),
    DOMICILE    (ApheticMark.DOMICILE_POINT);

    fun oppositeMark() : ApheticMark = ApheticMark.oppositeMark(this)

    override fun toString(): String {
        return "ApheticMark(point=$point)"
    }


    companion object {
        private const val DOMICILE_POINT = 6
        private const val EXALTATION_POINT = 5
        private const val KINSHIP_POINT = 4
        private const val NEUTRAL_POINT = 3
        private const val FEUD_POINT = 2
        private const val FALL_POINT = 1
        private const val DETRIMENT_POINT = 0

        fun oppositeMark(mark : ApheticMark) : ApheticMark {
            return when (mark) {
                DETRIMENT -> DOMICILE
                FALL -> EXALTATION
                FEUD -> KINSHIP
                NEUTRAL -> NEUTRAL
                KINSHIP -> FEUD
                EXALTATION -> FALL
                DOMICILE -> DETRIMENT
            }
        }
    }


}