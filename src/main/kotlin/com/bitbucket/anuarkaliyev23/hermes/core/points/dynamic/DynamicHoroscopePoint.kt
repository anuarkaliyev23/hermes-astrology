package com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic

import com.bitbucket.anuarkaliyev23.hermes.core.points.HoroscopePoint
import kotlin.math.abs

interface DynamicHoroscopePoint : HoroscopePoint {
    var speed : Double
    fun isDirect() : Boolean = speed >= 0

    companion object {
        private fun maxSimpleSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint): DynamicHoroscopePoint {
            return if (first.speed >= second.speed) first else second
        }

        private fun minSimpleSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint) : DynamicHoroscopePoint {
            return if (first.speed < second.speed) first else second
        }

        private fun maxAbsSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint) : DynamicHoroscopePoint {
            return if (abs(first.speed) >= Math.abs(second.speed)) first else second
        }

        private fun minAbsSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint) : DynamicHoroscopePoint {
            return if (abs(first.speed) < Math.abs(second.speed)) first else second
        }

        fun maxSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint, absolute: Boolean = true) : DynamicHoroscopePoint {
            return if (absolute) maxAbsSpeed(first, second) else maxSimpleSpeed(first, second)
        }
        fun minSpeed(first: DynamicHoroscopePoint, second: DynamicHoroscopePoint, absolute: Boolean = true) : DynamicHoroscopePoint {
            return if (absolute) minAbsSpeed(first, second) else minSimpleSpeed(first, second)
        }
    }
}