package com.bitbucket.anuarkaliyev23.hermes.core.exceptions

open class GeneralHermesException : Exception()