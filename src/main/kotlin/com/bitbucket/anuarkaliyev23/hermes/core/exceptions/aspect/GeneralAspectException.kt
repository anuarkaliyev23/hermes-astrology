package com.bitbucket.anuarkaliyev23.hermes.core.exceptions.aspect

import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.GeneralHermesException

open class GeneralAspectException : GeneralHermesException()