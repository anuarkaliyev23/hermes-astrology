package com.bitbucket.anuarkaliyev23.hermes.core.aspects

enum class AspectQuality {
    HARMONIOUS, STRAINING, CREATIVE, UNDETERMINED;
}