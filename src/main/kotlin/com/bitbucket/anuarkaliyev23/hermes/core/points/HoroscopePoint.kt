package com.bitbucket.anuarkaliyev23.hermes.core.points

interface HoroscopePoint {
    var position : HoroscopePosition

    operator fun minus(other: HoroscopePoint): Double {
        return this.position.aspect(other.position)
    }

}