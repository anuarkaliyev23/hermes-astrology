package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

enum class HouseQuality {
    ANGULAR,
    SUCCEDENT,
    CADENT;
}