package com.bitbucket.anuarkaliyev23.hermes.core.exceptions

class UnrecognizedSignException : GeneralHermesException()