package com.bitbucket.anuarkaliyev23.hermes.core.points.houses

enum class HouseSystem {
    PLACIDUS,
    REGIOMANTANUS,
    KOCH,
    TOPOCENTRIC,
    M_HOUSE,
    PORPHYRY,
    CARTERS_POLY_EQUATERIAL,
    MERIDIAN,
    MORINUS,
    CAMPANUS,
    SINUSOIDAL
}