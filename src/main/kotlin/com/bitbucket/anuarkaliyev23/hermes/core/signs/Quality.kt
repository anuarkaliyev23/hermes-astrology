package com.bitbucket.anuarkaliyev23.hermes.core.signs

enum class Quality {
    CARDINAL, FIXED, MUTABLE;
}