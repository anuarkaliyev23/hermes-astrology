package com.bitbucket.anuarkaliyev23.hermes.integration

interface ChartFactory: CosmogramFactory, HouseChartFactory, HouseGridFactory