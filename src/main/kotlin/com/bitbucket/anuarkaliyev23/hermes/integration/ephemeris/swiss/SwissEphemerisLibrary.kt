package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.sun.jna.Library

/**
 * Interface intended for JNA usage of Swiss Ephemeris.
 * Swiss Ephemeris is a famous ephemeris (i.e. celestial body positions) computation library written in C
 * This interface is using [Library] interface of JNA library to seamlessly map between Java and C Types
 * during JNI calls.
 * */
interface SwissEphemerisLibrary : Library {
    /**
     * Ephemeris computation for celestial body
     *
     * @param tjd_ut Julian day in UT (Universal Time)
     * @param ipl celestial body index number. Fixed constant. i.e [IPL_SE_SUN], [IPL_SE_MOON] etc.
     * @param iflag 32-bit integer (in original C Library) containing bit flags that indicate what kind of computation is needed
     * @param xx array of six doubles in which Swiss Ephemeris will store longitude, latitude, distance, speed in longitude, speed in distance
     * @param serr - array of 256 chars. Swiss Ephemeris will store any error message in them.
     *
     * @return result code. Any result < 0 will be indicating an occured error. Successful result will return flag bits indicating what kind of computation have occurred
     * */
    fun swe_calc_ut(tjd_ut: Double, ipl: Int, iflag: Int, xx: DoubleArray, serr: CharArray): Int


    /**
     * Swiss Ephemeris function used to convert UTC time to Julian Day.
     * Julian Day is a widely-used in astronomy/astrology date and time format.
     *
     * @param iyear UTC year
     * @param imonth UTC month
     * @param iday UTC day
     * @param ihour UTC hour
     * @param imin UTC minute
     * @param dsec UTC second (second in decimal format, so leap seconds could be used)
     * @param gregflag logically boolean value. If Gregorian Calendar is used must be equal to 1, if Julian Calendar is used must be equal to zero
     * @param dret array consisting of 2 doubles. Result will be stored in it. dret[0] = Julian day in ET (Eastern Time), dret[1] = Julian day in UT (Universal Time)
     * @param serr - array of chars in which any error message will be stored
     *
     * @return result code. Any result < 0 will be indicating an occured error. Successful result will return flag bits indicating what kind of computation have occured
     * */
    fun swe_utc_to_jd(iyear: Int, imonth: Int, iday: Int, ihour: Int, imin: Int, dsec: Double, gregflag: Int, dret: DoubleArray, serr: CharArray): Int

    /**
     * Swiss Ephemeris function used to compute house locations of various house systems.
     *
     * Most used codes for [hsys]:
     * 'P' - Placidus
     * 'K' - Koch
     * 'O' - Porphyrius
     * 'R' - Regiomontanus
     * 'C' - Campanus
     * 'A' or 'E' - Equal
     * 'W' - Whole Sign
     *
     * Placidus and Koch CANNOT BE COMPUTED BEYOND THE POLAR CIRCLE. In such cases Swiss Ephemerys will automatically switch to Porphyrius sstem instead.
     *
     * @param tjd Julian Day Number, Universal Time.
     * @param geolat Geographic latitude. In degrees. Northern latitude is positive, Southern latitude is negative.
     * @param geolon Geographic longitude. In degrees. Eastern longitude is positive, Western longitude is negative.
     * @param hsys House system index. Logical char, ASCII code of documented letter-constants. (P - Placidus etc.)
     * @param cusps Array of 13 (or 37 for system 'G') doubles intended to store houses coordinates. (cusps[0] will always be zero)
     * @param asmc Array of 10 doubles. In [asmc] parameter will be stored Ascendant, MC, ARMC, Vertex, equatorial ascendant, co-ascendant(W. Koch), co-ascendant(M. Munkasey), polar ascendant (M.Mukasey)s
     *
     * @return result code. Any result < 0 will be indicating an occured error. Successful result will return flag bits indicating what kind of computation have occurred
     * */
    fun swe_houses(tjd: Double, geolat: Double, geolon: Double, hsys: Int, cusps: DoubleArray, asmc: DoubleArray): Int

    companion object {
        /*Standard celestial objects*/
        const val IPL_SE_ECL_NUT = -1
        /**
         * Sun index in Swiss Ephemeris
         * */
        const val IPL_SE_SUN = 0

        /**
         * Moon index in Swiss Ephemeris
         * */
        const val IPL_SE_MOON = 1

        /**
         * Mercury index in Swiss Ephemeris
         * */
        const val IPL_SE_MERCURY = 2

        /**
         * Venus index in Swiss Ephemeris
         * */
        const val IPL_SE_VENUS = 3

        /**
         * Mars index in Swiss Ephemeris
         * */
        const val IPL_SE_MARS = 4

        /**
         * Jupiter index in Swiss Ephemeris
         * */
        const val IPL_SE_JUPITER = 5

        /**
         * Saturn index in Swiss Ephemeris
         * */
        const val IPL_SE_SATURN = 6

        /**
         * Uranus index in Swiss Ephemeris
         * */
        const val IPL_SE_URANUS = 7

        /**
         * Neptune index in Swiss Ephemeris
         * */
        const val IPL_SE_NEPTUNE = 8

        /**
         * Pluto index in Swiss Ephemeris
         * */
        const val IPL_SE_PLUTO = 9
        const val IPL_SE_MEAN_NODE = 10
        const val IPL_SE_TRUE_NODE = 11
        const val IPL_SE_MEAN_APOG = 12
        const val IPL_SE_OSCU_APOG = 13
        const val IPL_SE_EARTH = 14
        const val IPL_SE_CHIRON = 15
        const val IPL_SE_PHOLUS = 16
        const val IPL_SE_CERES = 17
        const val IPL_SE_PALLAS = 18
        const val IPL_SE_JUNO = 19
        const val IPL_SE_VESTA = 20
        const val IPL_SE_INTP_APOG = 21
        const val IPL_SE_INTP_PERG = 22
        const val IPL_SE_NPLANETS = 23
        const val IPL_SE_FICT_OFFSET = 40
        const val IPL_SE_NFICT_ELEM = 15
        const val IPL_SE_AST_OFFSET = 10000

        /* Hamburger or Uranian "planets" */
        const val IPL_SE_CUPIDO = 40
        const val IPL_SE_HADES = 41
        const val IPL_SE_ZEUS = 42
        const val IPL_SE_KRONOS = 43
        const val IPL_SE_APOLLON = 44
        const val IPL_SE_ADMETOS = 45
        const val IPL_SE_VULKANUS = 46
        const val IPL_SE_POSEIDON = 47

        /* other fictitious bodies */
        const val IPL_SE_ISIS = 48
        const val IPL_SE_NIBIRU = 49
        const val IPL_SE_HARRINGTON = 50
        const val IPL_SE_NEPTUNE_LEVERRIER = 51
        const val IPL_SE_NEPTUNE_ADAMS = 52
        const val IPL_SE_PLUTO_LOWELL = 53
        const val IPL_SE_PLUTO_PICKERING = 54

        /*House systems*/
        const val HSYS_ALCABITUS = 'B'.toInt()
        const val HSYS_APC = 'Y'.toInt()
        const val HSYS_AXIAL_ROTATION = 'X'.toInt()
        const val HSYS_AZIMUTHAL = 'H'.toInt()
        const val HSYS_CAMPANUS = 'C'.toInt()
        const val HSYS_CARTER = 'F'.toInt()
        const val HSYS_EQUAL = 'A'.toInt()
        const val HSYS_EQUAL_MC = 'D'.toInt()
        const val HSYS_EQUAL_SIGN = 'N'.toInt()
        const val HSYS_GAUQUELIN = 'G'.toInt()
        const val HSYS_SUNSHINE_TREINDL = 'I'.toInt()
        const val HSYS_SUNSHINE_MAKRANSKY = 'i'.toInt()
        const val HSYS_KOCH = 'K'.toInt()
        const val HSYS_KRUSINSKI = 'U'.toInt()
        const val HSYS_MORINUS = 'M'.toInt()
        const val HSYS_PLACIDUS = 'P'.toInt()
        const val HSYS_POLICH = 'T'.toInt()
        const val HSYS_PORPHYRIUS = 'P'.toInt()
        const val HSYS_PULLEN_SD = 'L'.toInt()
        const val HSYS_PULLEN_SR = 'Q'.toInt()
        const val HSYS_REGIOMONTANUS = 'R'.toInt()
        const val HSYS_SRIPATI = 'S'.toInt()
        const val HSYS_VEHLOW = 'V'.toInt()
        const val HSYS_WHOLE = 'W'.toInt()

        const val IFLAG_ALL: Int = 256
        const val DEFAULT_SERR_SIZE = 256
    }
}