package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseGrid
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime

interface SwissEphemerisWrapper {
    fun planetOf(dateTime: ZonedDateTime, planetType: PlanetType): Planet
            = planetOf(dateTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime(), planetType)

    fun planetOf(dateTime: LocalDateTime, planetType: PlanetType): Planet


    fun housesOf(dateTime: ZonedDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid
            = housesOf(dateTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime(), latitude, longitude, houseSystem)

    fun housesOf(dateTime: LocalDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid


    fun julianDayUT(dateTime: ZonedDateTime): Double
            = julianDayUT(dateTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime())

    fun julianDayUT(dateTime: LocalDateTime): Double
}