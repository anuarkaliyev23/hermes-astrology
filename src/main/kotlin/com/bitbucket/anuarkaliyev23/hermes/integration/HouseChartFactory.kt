package com.bitbucket.anuarkaliyev23.hermes.integration

import com.bitbucket.anuarkaliyev23.hermes.core.charts.HouseChart
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import java.time.ZonedDateTime

interface HouseChartFactory {
    fun houseChart(dateTime: ZonedDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseChart
}