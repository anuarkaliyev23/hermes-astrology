package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

interface SwissEphemerisLibraryFactory {
    fun library(): SwissEphemerisLibrary
}