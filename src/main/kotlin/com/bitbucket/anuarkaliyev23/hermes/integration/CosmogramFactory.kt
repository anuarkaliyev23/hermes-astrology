package com.bitbucket.anuarkaliyev23.hermes.integration

import com.bitbucket.anuarkaliyev23.hermes.core.charts.Cosmogram
import java.time.*

/**
 * Abstraction layer for computation of cosmogram using [java.time] classes introduced in Java 8.
 * */
interface CosmogramFactory {
    /**
     * Cosmogram computation method.
     *
     * @param dateTime - [LocalDateTime] of an event. Expected to be in Universal Timezone
     * @return cosmogram for that instance of time
     * */
    fun cosmogram(dateTime: LocalDateTime): Cosmogram

    /**
     * Cosmogram computation method
     *
     * @param dateTime - [ZonedDateTime] of an event.
     * @return cosmogram for that instance of time
     * */
    fun cosmogram(dateTime: ZonedDateTime): Cosmogram {
        return cosmogram(dateTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime())
    }

    /**
     * Cosmogram computation method.
     *
     * @param date - [LocalDate] of an event. Time will be assumed as midnight
     * @return cosmogram for that instance of time
     * */
    fun cosmogram(date: LocalDate): Cosmogram {
        return cosmogram(date.atStartOfDay())
    }
}