package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.logger
import com.sun.jna.Native
import com.sun.jna.Platform

class JnaSwissEphemerisLibraryFactory(libraryFilePath: String) : SwissEphemerisLibraryFactory {
    private val instance: SwissEphemerisLibrary

    init {
        logger().debug("Initializing JNASwissEphemerisLibraryFactory with libraryFilePath = [$libraryFilePath]")
        if (Platform.isWindows()) {
            logger().debug("Recognized OS as Windows. Loading SwissEphemerisLibrary")
            instance = Native.load(libraryFilePath, SwissEphemerisLibrary::class.java)
            logger().debug("Loaded SwissEphemerisLibrary from path = [$libraryFilePath]")
        } else {
            throw UnsupportedOperationException("No Platforms except Windows are supported")
        }
    }

    override fun library(): SwissEphemerisLibrary = instance
}