package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.charts.Cosmogram
import com.bitbucket.anuarkaliyev23.hermes.core.charts.HouseChart
import com.bitbucket.anuarkaliyev23.hermes.core.charts.NatalChart
import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toLocalDateTimeUTC
import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toZonedDateTimeUTC
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseGrid
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import com.bitbucket.anuarkaliyev23.hermes.integration.ChartFactory
import java.time.LocalDateTime
import java.time.ZonedDateTime

class SwissEphemerisChartFactory(
        private val wrapper: SwissEphemerisWrapper
): ChartFactory {
    private fun planets(dateTime: ZonedDateTime): List<Planet> = planets(dateTime.toLocalDateTimeUTC())
    private fun planets(dateTime: LocalDateTime): List<Planet> {
        return PlanetType.values().map { wrapper.planetOf(dateTime, it) }
    }

    override fun cosmogram(dateTime: LocalDateTime): Cosmogram {
        return Cosmogram(planets(dateTime), dateTime.toZonedDateTimeUTC())
    }

    override fun houseChart(dateTime: ZonedDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseChart {
        return NatalChart(planets(dateTime), houses(dateTime, latitude, longitude, houseSystem), dateTime)
    }

    override fun houses(dateTime: LocalDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid {
        return wrapper.housesOf(dateTime, latitude, longitude, houseSystem)
    }
}