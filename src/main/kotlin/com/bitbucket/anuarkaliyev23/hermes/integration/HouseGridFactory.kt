package com.bitbucket.anuarkaliyev23.hermes.integration

import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseGrid
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseSystem
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime

interface HouseGridFactory {
    fun houses(dateTime: ZonedDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid {
        return houses(dateTime.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime(), latitude, longitude, houseSystem)
    }
    fun houses(dateTime: LocalDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid
}