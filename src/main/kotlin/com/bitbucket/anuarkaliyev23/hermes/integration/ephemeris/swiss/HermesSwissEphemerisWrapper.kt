package com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss

import com.bitbucket.anuarkaliyev23.hermes.core.exceptions.SwissEphemerisFailureException
import com.bitbucket.anuarkaliyev23.hermes.core.extensions.logger
import com.bitbucket.anuarkaliyev23.hermes.core.extensions.toHoroscopePosition
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.PlanetType
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.*
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.DEFAULT_SERR_SIZE
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.HSYS_KOCH
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.HSYS_PLACIDUS
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.HSYS_REGIOMONTANUS
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IFLAG_ALL
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_JUPITER
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_MARS
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_MERCURY
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_MOON
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_NEPTUNE
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_PLUTO
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_SATURN
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_SUN
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_URANUS
import com.bitbucket.anuarkaliyev23.hermes.integration.ephemeris.swiss.SwissEphemerisLibrary.Companion.IPL_SE_VENUS
import java.time.LocalDateTime
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class HermesSwissEphemerisWrapper(
        swissEphemerisLibraryFactory: SwissEphemerisLibraryFactory
): SwissEphemerisWrapper {
    private val library = swissEphemerisLibraryFactory.library()
    private val lock = ReentrantLock()

    private fun indexOf(planetType: PlanetType): Int {
        return when (planetType) {
            PlanetType.SUN -> IPL_SE_SUN
            PlanetType.MOON -> IPL_SE_MOON
            PlanetType.MERCURY -> IPL_SE_MERCURY
            PlanetType.VENUS -> IPL_SE_VENUS
            PlanetType.MARS -> IPL_SE_MARS
            PlanetType.JUPITER -> IPL_SE_JUPITER
            PlanetType.SATURN -> IPL_SE_SATURN
            PlanetType.URANUS -> IPL_SE_URANUS
            PlanetType.NEPTUNE -> IPL_SE_NEPTUNE
            PlanetType.PLUTO -> IPL_SE_PLUTO
        }
    }

    private fun houseSystemIndexOf(houseSystem: HouseSystem): Int {
        return when (houseSystem) {
            HouseSystem.PLACIDUS -> HSYS_PLACIDUS
            HouseSystem.KOCH -> HSYS_KOCH
            HouseSystem.REGIOMANTANUS -> HSYS_REGIOMONTANUS
            else -> throw UnsupportedOperationException("[$houseSystem] House System is not supported at the moment.")
        }
    }

    override fun planetOf(dateTime: LocalDateTime, planetType: PlanetType): Planet {
        logger().debug("Computing planet for dateTime=[$dateTime], planet=[$planetType]")
        val julianDay = julianDayUT(dateTime)

        lock.withLock {
            val planetIndex = indexOf(planetType)
            logger().debug("Swiss Ephemeris planet index for [$planetType] = [$planetIndex] ")

            val resultArray = DoubleArray(6)
            val errorMessage = CharArray(256)

            val result = library.swe_calc_ut(julianDay, planetIndex, IFLAG_ALL, resultArray, errorMessage)

            if (result < 0) {
                throw SwissEphemerisFailureException(String(errorMessage))
            }
            logger().debug("[$planetType] for [$dateTime] properties = [${resultArray.toList()}]")
            val position = resultArray[0]
            val speed = resultArray[3]
            return Planet(planetType, position.toHoroscopePosition(), speed)
        }
    }

    override fun housesOf(dateTime: LocalDateTime, latitude: Double, longitude: Double, houseSystem: HouseSystem): HouseGrid {
        logger().debug("Computing houses for [$dateTime], [$latitude, $longitude], [$houseSystem]")
        val julianDay = julianDayUT(dateTime)
        lock.withLock {
            val houseSystemIndex = houseSystemIndexOf(houseSystem)
            val cusps = DoubleArray(13)
            val asmc = DoubleArray(10)
            val result = library.swe_houses(julianDay, latitude, longitude, houseSystemIndex, cusps, asmc)
            if (result < 0) {
                throw SwissEphemerisFailureException("Could not compute houses for [$julianDay], [$latitude], [$longitude], [$houseSystemIndex]")
            }
            logger().debug("Houses cusps coordinates of [$dateTime][$latitude][$longitude]= [${cusps.toList()}]")
            logger().debug("ASMC of [$dateTime][$latitude][$longitude]= [${asmc.toList()}]")

            val houses = cusps
                    .filterIndexed { index, _ ->  index > 0}
                    .mapIndexed {index, house -> House(HouseIndex.values()[index], house.toHoroscopePosition()) }
                    .toSortedSet()

            logger().debug("House objects = [$houses]")
            return SimpleHouseGrid(houses, houseSystem)
        }
    }

    override fun julianDayUT(dateTime: LocalDateTime): Double {
        logger().debug("Computing Julian Day for dateTime = [$dateTime]")
        lock.withLock {
            val year = dateTime.year
            val month = dateTime.monthValue
            val day = dateTime.dayOfMonth
            val hour = dateTime.hour
            val minute = dateTime.minute
            val seconds = dateTime.second.toDouble()

            val resultArray = DoubleArray(2)
            val errorMessage = CharArray(DEFAULT_SERR_SIZE)

            val result = library.swe_utc_to_jd(year, month, day, hour, minute, seconds, 1, resultArray, errorMessage)

            if (result < 0) {
                throw SwissEphemerisFailureException(String(errorMessage))
            }
            logger().debug("Got Julian days (ET, UT) = [${resultArray.toList()}]")
            return resultArray[1]
        }
    }

}