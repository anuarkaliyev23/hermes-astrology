package com.bitbucket.anuarkaliyev23.hermes.gui.fx

import javafx.application.Application

fun main() {
    Application.launch(HermesApplication::class.java)
}