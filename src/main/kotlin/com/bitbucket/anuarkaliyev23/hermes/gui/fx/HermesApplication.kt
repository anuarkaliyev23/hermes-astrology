package com.bitbucket.anuarkaliyev23.hermes.gui.fx

import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.stage.Stage

class HermesApplication : Application() {
    override fun start(stage: Stage) {
        val root = Group()
        val canvas = Canvas(800.0, 600.0)
        canvas.translateX = canvas.width / 2
        canvas.translateY = canvas.height / 2


        root.children.add(canvas)
        val scene = Scene(root)
        stage.scene = scene
        stage.show()
    }
}