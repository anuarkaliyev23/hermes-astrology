package com.bitbucket.anuarkaliyev23.hermes.gui.svg

import com.bitbucket.anuarkaliyev23.hermes.core.aspects.Aspect
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.AspectQuality
import com.bitbucket.anuarkaliyev23.hermes.core.aspects.OrbTable
import com.bitbucket.anuarkaliyev23.hermes.core.charts.HouseChart
import com.bitbucket.anuarkaliyev23.hermes.core.charts.SingleChart
import com.bitbucket.anuarkaliyev23.hermes.core.points.dynamic.Planet
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.House
import com.bitbucket.anuarkaliyev23.hermes.core.points.houses.HouseIndex
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Element
import com.bitbucket.anuarkaliyev23.hermes.core.signs.Sign

class ChartSVGDrawer(
        val zodiacWheelRadius: Double,
        val zodiacSymbolsRadius: Double,
        val planetCircleRadius: Double,
        val aspectCircleRadius: Double,
        val houseCuspsRadius: Double,
        val houseIndexRadius: Double
) {
    private fun toZodiacCoordinate(coordinate: Double): Double {
        return (coordinate - 180) * -1
    }
    private fun toZodiacCoordinate(polarCoordinate: PolarCoordinate): PolarCoordinate {
        return PolarCoordinate(polarCoordinate.radius, toZodiacCoordinate(polarCoordinate.angle))
    }

    private fun signPolarCoordinates(radius: Double, sign: Sign): PolarCoordinate {
        return PolarCoordinate(radius, toZodiacCoordinate(sign.startPoint.toDouble()))
    }

    private fun signWheel(radius: Double, sign: Sign) : String{
        val polarStart = signPolarCoordinates(radius, sign)
        val polarEnd = signPolarCoordinates(radius, sign.nextSign())
        val color = when (sign.element) {
            Element.FIRE -> "red"
            Element.EARTH -> "orange"
            Element.AIR -> "aquamarine"
            Element.WATER -> "blue"
        }

        return """
            <!--$sign. Polar(radius, ${sign.startPoint} - ${sign.nextSign().startPoint})-->
            <!-- ($polarStart - $polarEnd). Cartesian (${polarStart.toCartesian()} - ${polarEnd.toCartesian()})-->
            
            <path d="
                M 0 0
                L ${polarStart.toCartesian().x} ${polarStart.toCartesian().y}
                L ${polarEnd.toCartesian().x} ${polarEnd.toCartesian().y}
                Z
            " fill="$color"/>
            
        """.trimIndent()
    }

    private fun signsWheel(radius: Double): String {
        val paths= Sign.values().map {signWheel(radius, it)}
        val builder = StringBuilder()
        paths.forEach {
            builder.append(it)
        }
        return builder.toString()
    }

    private fun zodiacWheel(radius: Double = 100.0, stroke: String = "black", strokeWidth: Double = 0.15, fillOpacity: Double = 0.6): String {
        return """
            <g fill-opacity="$fillOpacity" stroke="$stroke" stroke-width="${strokeWidth}px">
                ${signsWheel(radius)}
            </g>
        """.trimIndent()
    }

    private fun zodiacSignSymbol(radius: Double, sign: Sign): String {
        val polarStart = signPolarCoordinates(radius, sign)
        val polarMiddle = PolarCoordinate(polarStart.radius, polarStart.angle - 15)
        return """
            <text x="${polarMiddle.toCartesian().x}" y="${polarMiddle.toCartesian().y}">${sign.symbol}</text>
        """.trimIndent()
    }

    private fun zodiacSignSymbols(radius: Double, stroke: String = "black", strokeWidth: Double = 0.5): String {
        val strings = Sign.values().map { zodiacSignSymbol(radius, it) }.foldRight("", {s, acc -> acc + s})
        return """
             <g fill-opacity="1" stroke="$stroke" stroke-width="${strokeWidth}px" text-anchor="middle">
                $strings 
            </g>
            
        """.trimIndent()
    }

    private fun planetSymbol(planet: Planet, planetSymbolRadius: Double, planetDotRadius: Double, planetDotSize: Double): String {
        val planetPolarCoordinate = PolarCoordinate(planetSymbolRadius, toZodiacCoordinate(planet.position.coordinate))
        val dotPolarCoordinate = PolarCoordinate(planetDotRadius, toZodiacCoordinate(planet.position.coordinate))
        return """
            <g>
                <text x="${planetPolarCoordinate.toCartesian().x}" y="${planetPolarCoordinate.toCartesian().y}" text-anchor="middle">${planet.type.unicodeSymbol}</text>
                <circle cx="${dotPolarCoordinate.toCartesian().x}" cy="${dotPolarCoordinate.toCartesian().y}" r="$planetDotSize" fill="black" />
            </g>
            
        """.trimIndent()
    }

    private fun planetsSymbols(planets: List<Planet>, planetSymbolRadius: Double, planetDotRadius: Double, planetDotSize: Double, fillOpacity: Double, strokeWidth: Double): String {
        val planetsString = planets.map { planetSymbol(it, planetSymbolRadius, planetDotRadius, planetDotSize) }
        val planetsFold = planetsString.foldRight("", {s, acc -> acc + s })
        return """
            <g fill-opacity="$fillOpacity" stroke="black" stroke-width="$strokeWidth" fill="black">
                $planetsFold
            </g>
            
        """.trimIndent()
    }

    private fun aspect(aspect: Aspect, planetDotRadius: Double): String {
        val color = when (aspect.type()?.quality) {
            AspectQuality.STRAINING -> "blue"
            AspectQuality.HARMONIOUS -> "red"
            else -> "purple"
        }

        val first = PolarCoordinate(planetDotRadius, toZodiacCoordinate(aspect.first.position.coordinate))
        val second = PolarCoordinate(planetDotRadius, toZodiacCoordinate(aspect.second.position.coordinate))
        return """
            <line x1="${first.toCartesian().x}" y1="${first.toCartesian().y}" x2="${second.toCartesian().x}" y2="${second.toCartesian().y}" stroke="$color"/>
            
        """.trimIndent()
    }

    private fun aspects(aspects: List<Aspect>, planetDotsRadius: Double, strokeWidth: Double = 0.5, strokeOpacity: Double = 0.5): String {

        val aspectsList = aspects.map { aspect(it, planetDotsRadius )  }
        val folded = aspectsList.foldRight("", { s, acc -> acc + s })
        return """
            <g stroke-width="$strokeWidth" stroke-opacity="$strokeOpacity">
                $folded
            </g>
            
        """.trimIndent()
    }

    private fun innerCircle(radius: Double, stroke: String, fill: String, strokeWidth: Double, opacity: Double): String {
        return """
            <circle cx="0" cy="0" r="$radius" fill="$fill" opacity="$opacity" stroke="$stroke" stroke-width="$strokeWidth"/>
        """.trimIndent()
    }

    private fun house(house: House, houseCuspsRadius: Double, houseIndexRadius: Double): String {
        val strokeWidth = when (house.index) {
            HouseIndex.I -> 1.0
            HouseIndex.VII -> 1.0
            HouseIndex.IV -> 1.0
            HouseIndex.X -> 1.0
            else -> 0.5
        }

        val color = when(house.index) {
            HouseIndex.I -> "blue"
            HouseIndex.VII -> "blue"
            HouseIndex.IV -> "purple"
            HouseIndex.X -> "purple"
            else -> "black"
        }

        val position = PolarCoordinate(houseCuspsRadius, toZodiacCoordinate(house.position.coordinate))
        val indexPosition = PolarCoordinate(houseIndexRadius, toZodiacCoordinate(house.position.coordinate))

        val houseMC = """
            <circle cx="${position.toCartesian().x}" cy="${position.toCartesian().y}" r="3" stroke="$color" fill="$color" fill-opacity="1"/>
        """.trimIndent()

        return """
            <line x1="0" y1="0" x2="${position.toCartesian().x}" y2="${position.toCartesian().y}" stroke="$color" stroke-width="$strokeWidth"/>
            ${if (house.index == HouseIndex.X) {
                houseMC
            } else ""}
            <text x="${indexPosition.toCartesian().x}" y="${indexPosition.toCartesian().y}" text-anchor="middle">${house.index}</text>
        """.trimIndent()
    }

    private fun houses(houses: Set<House>, houseCuspsRadius: Double, houseIndexRadius: Double, stroke: String = "black", fillOpacity: Double = 0.5): String {
        return """
            <g fill-opacity="$fillOpacity" stroke="$stroke">
                ${houses.map { house(it, houseCuspsRadius, houseIndexRadius) }}
            </g>
        """.trimIndent()
    }

    fun svg(chart: SingleChart, orbTable: OrbTable): String {
        val zodiac = zodiacWheel(zodiacWheelRadius)
        val zodiacSymbols = zodiacSignSymbols(zodiacSymbolsRadius, "black", 1.0)
        val planets = planetsSymbols(chart.planets, planetCircleRadius - 10, aspectCircleRadius, 2.0, 1.0, 0.5)

        val innerCircle = innerCircle(aspectCircleRadius, "black", "white", 1.0, 1.0)
        val planetCircle = innerCircle(planetCircleRadius , "black", "white", 1.0, 0.8)
        val aspects = aspects(chart.aspects(orbTable).filter { it.type() != null } .toList(), aspectCircleRadius)

        val houseGrid = if (chart is HouseChart) chart.houseGrid else null
        val houses = if (houseGrid != null) {
            houses(houseGrid.houses, houseCuspsRadius, houseIndexRadius)
        } else ""

        return """
            <g transform="translate(250, 250)">
                $zodiac
                $planetCircle
                $houses
                $zodiacSymbols
                $innerCircle
                $planets
                $aspects
            </g>
            
        """.trimIndent()
    }

    fun wrappedSVG(chart: SingleChart, orbTable: OrbTable): String {
        val svg = """
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
                <svg version = "1.1"
                     baseProfile="full"
                     xmlns = "http://www.w3.org/2000/svg"
                     height = "500px"  width = "500px">
                    
                    ${svg(chart, orbTable)}
                    
                </svg>
        """.trimIndent()

        return svg
    }

}

