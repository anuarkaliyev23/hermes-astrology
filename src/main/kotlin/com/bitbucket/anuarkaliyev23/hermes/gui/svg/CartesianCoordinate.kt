package com.bitbucket.anuarkaliyev23.hermes.gui.svg

data class CartesianCoordinate(
        val x: Double,
        val y: Double
)