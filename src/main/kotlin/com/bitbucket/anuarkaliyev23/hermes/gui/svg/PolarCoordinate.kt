package com.bitbucket.anuarkaliyev23.hermes.gui.svg

import com.bitbucket.anuarkaliyev23.hermes.core.extensions.logger
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

data class PolarCoordinate(
        val radius: Double,
        val angle: Double
) {
    fun toCartesian(): CartesianCoordinate {
        val radiansX = angle * PI / 180
        val radianY = angle * PI / 180

        val x = radius * cos(radiansX)
        val y = radius * sin(radianY)
        val cartesianCoordinate = CartesianCoordinate(x, y)
        logger().debug("$this -> $cartesianCoordinate")
        return cartesianCoordinate
    }
}