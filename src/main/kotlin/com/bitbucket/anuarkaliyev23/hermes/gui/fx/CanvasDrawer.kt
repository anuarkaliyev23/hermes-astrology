package com.bitbucket.anuarkaliyev23.hermes.gui.fx

import javafx.scene.canvas.Canvas

interface CanvasDrawer {
    fun draw(canvas: Canvas)
}